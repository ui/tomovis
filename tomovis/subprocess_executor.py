#!/usr/bin/env python
# coding: utf-8
# License: MIT (c) ESRF 2021
"""Executor running subprocess commands

Related projects/code:
- https://github.com/coleifer/huey/
- https://github.com/noxdafox/pebble
- https://gitlab.esrf.fr/workflow/ewoks/ewoksorange/-/blob/main/ewoksorange/bindings/taskexecutor_queue.py
"""
import atexit
import concurrent.futures
import enum
import queue
import subprocess
import threading
import weakref

__all__ = ["SubprocessExecutor"]


class CancellableFuture(concurrent.futures.Future):
    """A Future object that can be cancelled while running"""

    def __init__(self):
        super().__init__()
        self._running_callbacks = []

    def add_running_callback(self, fn):
        """Attaches a callable that will be called when the future starts running.

        :param callable fn:
            A callable that will be called with this future as its only
            argument when the future starts running.
        """
        with self._condition:
            if self._state == "PENDING":
                self._running_callbacks.append(fn)
                return
            if self._state not in ["RUNNING", "FINISHED"]:
                return  # No-op
        try:
            fn(self)
        except Exception:
            pass

    def set_running_or_notify_cancel(self):
        if super().set_running_or_notify_cancel():
            for callback in self._running_callbacks:
                try:
                    callback(self)
                except Exception:
                    pass
            return True
        return False

    def cancel(self) -> bool:
        with self._condition:
            if self._state != "RUNNING":
                return super().cancel()

            self._state = "CANCELLED"
            self._condition.notify_all()
        self._invoke_callbacks()
        return True


class SubprocessExecutor(threading.Thread):
    """Executor of commands through subprocess.Popen"""

    STOP_TIMEOUT = 1

    class _State(enum.Enum):
        """Running state of the executor"""

        RUNNING = enum.auto()
        PENDING = enum.auto()  # Pending on submitted jobs to shutdown
        SHUTDOWN = enum.auto()

    def __init__(self):
        super().__init__()
        self.__queue = queue.SimpleQueue()
        self.__state = self._State.RUNNING
        self.__current_task = None
        self.daemon = True
        atexit.register(SubprocessExecutor.__atexit, weakref.ref(self))

        self.start()

    def __enter__(self):
        return self

    def __exit__(self, type, value, traceback):
        self.shutdown()

    @staticmethod
    def __atexit(ref):
        """Handle closing during Python termination"""
        executor = ref()
        if executor is None:
            return
        executor.shutdown(wait=False, cancel_futures=True)

    def __cancel_futures(self):
        """Cancel pending and running futures"""
        while True:  # Empty the queue
            try:
                task = self.__queue.get(block=False)
            except queue.Empty:
                break
            task[0].cancel()

        # Cancel currently running job if any
        current_task = self.__current_task  # Atomic
        if current_task is not None:
            current_task[0].cancel()

    def shutdown(self, wait: bool = True, cancel_futures=False):
        """Stop the executor

        :param bool wait:
            True (default) to wait for termination, False to return directly.
        :param bool cancel_futures:
            True to cancel all pending and running futures, default: False.
        """
        if self.__state is self._State.RUNNING:
            if not cancel_futures:
                self.__state = self._State.PENDING
            else:
                self.__state = self._State.SHUTDOWN
                self.__cancel_futures()
            self.__queue.put(None)  # Put an empty message
            atexit.unregister(self.__atexit)
            if wait:
                self.join()

    def __del__(self):
        self.shutdown(wait=False, cancel_futures=True)

    def run(self):
        while self.__state is not self._State.SHUTDOWN:
            try:
                self.__current_task = self.__queue.get(
                    block=True, timeout=self.STOP_TIMEOUT
                )  # Atomic =
            except queue.Empty:
                continue

            if self.__current_task is None:
                return  # Received termination message, returning

            future, args, kwargs = self.__current_task
            self.__run_subprocess(future, *args, **kwargs)
            self.__current_task = None  # Atomic

    def __run_subprocess(self, future, *args, **kwargs):
        if not future.set_running_or_notify_cancel():
            return

        process = subprocess.Popen(*args, **kwargs)
        # Handle cancellation while running
        future.add_done_callback(
            lambda future: process.terminate() if future.cancelled() else None
        )

        while self.__state is not self._State.SHUTDOWN:
            try:
                process.wait(self.STOP_TIMEOUT)
            except subprocess.TimeoutExpired:
                pass
            else:
                # TODO handle stdout, stderr
                if not future.cancelled():
                    future.set_result(
                        subprocess.CompletedProcess(process.args, process.returncode)
                    )
                return
        future.cancel()  # Processing stopped, cancel future

    def submit(self, *args, **kwargs) -> CancellableFuture:
        """Submit a request to run a command through subprocess.Popen.

        For argument documentation, see:
        https://docs.python.org/3/library/subprocess.html#subprocess.Popen
        """
        if self.__state is not self._State.RUNNING:
            raise RuntimeError("SubprocessExecutor shutdown")
        future = CancellableFuture()
        self.__queue.put((future, args, kwargs))
        return future
