#!/usr/bin/env python
# coding: utf-8
# License: MIT (c) ESRF 2021
"""Create a tiled image from a scan"""
import logging
import pathlib
from typing import Optional, NamedTuple, Sequence, Tuple, Union

import h5py
import numpy
import pint
import traceback
from silx.utils import retry as retry_mod


from silx.io import h5py_utils
from tomo.helpers.tiling_reader import TechniqueInfo, TilingScanReader, TiledPlane


_logger = logging.getLogger(pathlib.Path(__file__).name)

UNITS = "mm"


class CopiedArea(NamedTuple):
    """Described the slices copie from src to dst by the `blit` function"""
    shape: Tuple[int]
    src_offset: Tuple[int]
    dst_offset: Tuple[int]

    def src_slices(self) -> Tuple[slice]:
        return tuple(slice(offset, offset + size) for offset, size in zip(self.src_offset, self.shape))

    def dst_slices(self) -> Tuple[slice]:
        return tuple(slice(offset, offset + size) for offset, size in zip(self.dst_offset, self.shape))


def blit(
    src: numpy.ndarray, dst: Union[numpy.ndarray, h5py.Dataset], offset: Sequence[int]
) -> Optional[CopiedArea]:
    """Copy `src` array into `dst` at given `offset` position.

    This functions avoids errors in case `src` is partly of completely outside `dst`.

    Warning: Negative offset are NOT understand as usual.
    Instead they are understand as coordinates in `dst` frame of reference.

    Information on effectively copied area is returned.
    """
    assert src.ndim == dst.ndim == len(offset)

    src_offset = -numpy.minimum(offset, 0)
    dst_offset = numpy.maximum(offset, 0)
    shape = numpy.clip(src.shape - src_offset, 0, dst.shape - dst_offset)
    if 0 in shape:
        return None
    copied_area = CopiedArea(tuple(shape), tuple(src_offset), tuple(dst_offset))
    dst[copied_area.dst_slices()] = src[copied_area.src_slices()]
    return copied_area


def binning(image: numpy.ndarray) -> numpy.ndarray:
    """2D 2x2 array binning"""
    assert image.ndim == 2
    return 0.25 * (
        image[:-1:2, :-1:2]
        + image[:-1:2, 1::2]
        + image[1::2, :-1:2]
        + image[1::2, 1::2]
    )


def create_dataset_with_units(group: h5py.Group, path: str, value, units: str) -> None:
    if isinstance(value, pint.Quantity):
        value = value.to(units).magnitude
    group[path] = value
    group[path].attrs["units"] = units


def prepare_histogram_nxdata(
    ingroup: h5py.Group,
    nbins: int,
    path: str = "histogram",
) -> h5py.Group:
    """Prepare a group `ingroup[path]` for storing a histogram as a NXdata"""
    group = ingroup.create_group(path)
    group.create_dataset("count", shape=(nbins,), dtype=numpy.int64, fillvalue=0)
    group.create_dataset(
        "bin_edges", shape=(nbins + 1,), dtype=numpy.float64, fillvalue=numpy.nan
    )
    group.create_dataset(
        "centers", shape=(nbins,), dtype=numpy.float64, fillvalue=numpy.nan
    )
    group.attrs["NX_class"] = "NXdata"
    group.attrs["signal"] = "count"
    group.attrs["axes"] = "centers"
    group.attrs["title"] = "Histogram"
    return group


def fill_histogram_nxdata(
    group: h5py.Group,
    bin_count: numpy.ndarray,
    bin_edges: numpy.ndarray,
) -> None:
    """Fill a prepared group with given histogram"""
    assert bin_count.ndim == bin_edges.ndim == 1
    nbins = len(bin_count)
    assert nbins + 1 == len(bin_edges)

    count_dataset = group["count"]
    bin_edges_dataset = group["bin_edges"]
    centers_dataset = group["centers"]

    assert len(count_dataset) == nbins
    assert len(bin_edges_dataset) == nbins + 1
    assert len(centers_dataset) == nbins

    count_dataset[()] = bin_count
    bin_edges_dataset[()] = bin_edges
    centers_dataset[()] = 0.5 * (bin_edges[:-1] + bin_edges[1:])

    count_dataset.flush()
    bin_edges_dataset.flush()
    centers_dataset.flush()


class HistogramAccumulator:
    """Histogram accumulator.

    Histogram is re-gridded if needed
    """

    def __init__(self, bins: int):
        self.__bin_edges = None
        self.__count = numpy.zeros((bins,), dtype=int)

    def update(self, data: numpy.ndarray):
        """Accumulate `data` into histogram, extending range if needed"""
        min_, max_ = numpy.nanmin(data), numpy.nanmax(data)
        if self.__bin_edges is None:
            _logger.debug("Initialize bin_edges")
            self.__count, self.__bin_edges = numpy.histogram(
                data, bins=len(self.__count), range=(min_, max_)
            )
            return

        previous_min = self.__bin_edges[0]
        previous_max = self.__bin_edges[-1]
        if min_ < previous_min or max_ > previous_max:
            _logger.debug("Extend bin_edges")
            self.__count, self.__bin_edges = numpy.histogram(
                self.centers,
                weights=self.__count,
                bins=len(self.__count),
                range=(min(min_, previous_min), max(max_, previous_max)),
            )

        self.__count += numpy.histogram(data, self.__bin_edges)[0]

    @property
    def bin_edges(self) -> numpy.ndarray:
        if self.__bin_edges is None:
            return numpy.zeros((len(self.__count),), dtype=numpy.float64)
        else:
            return numpy.array(self.__bin_edges)

    @property
    def centers(self) -> numpy.ndarray:
        edges = self.bin_edges
        return 0.5 * (edges[:-1] + edges[1:])

    @property
    def bin_count(self) -> numpy.ndarray:
        return numpy.array(self.__count)


def compute_nlevels(shape: Sequence[int], atleast_powerof2: int) -> int:
    """Returns number of levels in pyramid of details for given shape and constraint.

    `atleast_powerof2` is the power of 2 of the image size where to cut the pyramid.
    """
    shape_array = numpy.asarray(shape)

    if numpy.any(shape_array <= 0):
        return 1  # Base of the pyramid is empty

    # First, find level in the pyramid of details where the smallest dimension is 1
    max_level = numpy.min(numpy.log2(shape_array).astype(int))
    return (
        max_level
        + 1  # At least one level
        - numpy.clip(  # Cut pyramid if possible based on atleast_powerof2
            atleast_powerof2
            - numpy.ceil(numpy.log2(numpy.max(shape_array // 2**max_level))).astype(
                int
            ),
            0,
            max_level,
        )
    )


def prepare_tiling_dataset(
    group: h5py.Group,
    info: TechniqueInfo,
    side: str,
    nbins: int,
    dtype=numpy.float32,
    fillvalue=None,
):
    _logger.debug("Read scan information")
    pixel_size = info.pixel_size
    tiling_scan_info = getattr(info, side)
    viewpoint = tiling_scan_info.viewpoint
    y_range = tiling_scan_info.horizontal_range
    if viewpoint == "left":  # Invert coordinates
        y_range = -y_range[1], -y_range[0]
    z_range = tiling_scan_info.vertical_range
    rotation_offset = info.rotation_offset
    rotation_sign = info.rotation_sign

    # Get full image shape and corrected coordinate ranges
    full_shape = numpy.array(
        (
            int(numpy.ceil(numpy.abs(z_range[1] - z_range[0]) / pixel_size)),
            int(numpy.ceil(numpy.abs(y_range[1] - y_range[0]) / pixel_size)),
        )
    )
    y_range = y_range[0], y_range[0] + full_shape[1] * pixel_size
    z_range = z_range[0], z_range[0] + full_shape[0] * pixel_size

    _logger.debug(
        f"Save image calibration: pixel size: {pixel_size}{UNITS}, y: {y_range} {UNITS}, z: {z_range} {UNITS}"
    )
    group["viewpoint"] = viewpoint
    create_dataset_with_units(group, "pixel_size", pixel_size, UNITS)
    create_dataset_with_units(group, "y", y_range, UNITS)
    create_dataset_with_units(group, "z", z_range, UNITS)
    create_dataset_with_units(group, "rotation_offset", rotation_offset, "deg")
    group["rotation_sign"] = rotation_sign

    # Get number of levels by cutting levels where images have all dim smaller than 1024
    nlevel = compute_nlevels(full_shape, 9)
    _logger.debug(f"Number of level datasets: {nlevel}")

    datasets = []
    layer_names = []
    for level in range(nlevel):
        name = f"level{level}"
        layer_names.insert(0, name)
        level_shape = tuple(full_shape // (2**level))
        _logger.debug(
            f"Create dataset {name}: shape: {level_shape}, dtype: {numpy.dtype(dtype).name}"
        )
        datasets.append(group.create_dataset(name, shape=level_shape, dtype=dtype, fillvalue=fillvalue))

    group["definition"] = "tiling"
    group.attrs["NX_class"] = "NXentry"
    group["layers"] = layer_names

    prepare_histogram_nxdata(group, nbins)

    return datasets


def _is_h5py_exception(e):
    for frame in traceback.walk_tb(e.__traceback__):
        if frame[0].f_globals.get("__package__", None) == "h5py":
            return True
    return False


def _retry_h5py_error(e):
    """
    :param BaseException e:
    :returns bool:
    """
    if _is_h5py_exception(e):
        if isinstance(e, (OSError, RuntimeError)):
            return True
        elif isinstance(e, KeyError):
            # For example this needs to be retried:
            # KeyError: 'Unable to open object (bad object header version number)'
            return "Unable to open object" in str(e)
    elif isinstance(e, retry_mod.RetryError):
        return True
    return False


def _retry_on_error(e):
    """Add IndexError and missing file to retry exceptions"""
    # if h5py_utils._is_h5py_exception(e):
    if _is_h5py_exception(e):
        if isinstance(e, (FileNotFoundError, OSError)):
            return True
    if isinstance(e, IndexError):
        return True
    # return h5py_utils._retry_h5py_error(e)
    return _retry_h5py_error(e)


@h5py_utils.retry(retry_timeout=30, retry_period=2, retry_on_error=_retry_on_error)
def wait_sequence_start(filename: str, sequence_scan: str):
    # Make sure scan has started
    try:
        with h5py.File(filename, "r", locking=False) as h5file:
            start_time = h5file[sequence_scan + "/start_time"].asstr()[()]
    except BaseException as e:
        _logger.warning("Tiling sequence not started")
        raise e
    if not start_time:
        raise RuntimeError("Tiling sequence not started")


# Dark scan is expected to finish within 2 minutes
@h5py_utils.retry(retry_timeout=None, retry_period=2, retry_on_error=_retry_on_error)
def wait_tiling_scan_reader(filename: str, sequence_scan: str):
    reader = TilingScanReader(filename, sequence_scan)
    try:
        reader.info  # Accessing info fails is dark scan is not completed yet
    except BaseException as e:
        if reader.is_finished:  # Escape retry
            raise RuntimeError("Tiling scan ended")
        _logger.info(f"Waiting tiling information from {filename}:{sequence_scan}: {e}")
        raise e
    _logger.debug("Got tiling information")
    return reader


# No timeout: it relies on the sequence scan "end_time"
@h5py_utils.retry(retry_timeout=None, retry_period=2, retry_on_error=_retry_on_error)
def wait_tiled_plane(reader: TilingScanReader, side: str):
    try:
        tiled_plane = getattr(reader, side)
    except BaseException as e:
        if reader.is_finished:  # Escape retry
            raise RuntimeError("Tiling scan ended")
        _logger.info(f'Waiting "{side}" tiling scan: {e}')
        raise e
    _logger.debug(f'Got "{side}" tiled plane')
    return tiled_plane


# This is retrying for some time without checking the sequence end
@h5py_utils.retry(retry_timeout=10, retry_period=2, retry_on_error=_retry_on_error)
def _wait_corrected_image_after_sequence_end(tiled_plane: TiledPlane, index: str):
    try:
        result = tiled_plane.corrected_image(index)
    except BaseException as e:
        _logger.info(f"Waiting image (sequence ended): {e}")
        raise e
    return result


# No timeout: it relies on the sequence scan "end_time"
# Once finished, it retries a few times in case all images were not yet available
@h5py_utils.retry(retry_timeout=None, retry_period=2, retry_on_error=_retry_on_error)
def wait_corrected_image(tiled_plane: TiledPlane, index: str):
    try:
        result = tiled_plane.corrected_image(index)
    except BaseException as e:
        if tiled_plane.reader.is_finished:
            try:
                result = _wait_corrected_image_after_sequence_end(tiled_plane, index)
            except BaseException:  # Escape retry
                raise RuntimeError("Tiling scan ended")
        _logger.info(f"Waiting corrected image {index}: {e}")
        raise e
    _logger.debug(f"Got corrected image: {index}")
    return result


def fill_tiling_dataset(
    group: h5py.Group,
    tiled_plane: TiledPlane,
    datasets: Sequence[h5py.Dataset],
    histogram: HistogramAccumulator,
    histogram_group: h5py.Group,
    nbins: int,
):

    _logger.debug("Read scan information")
    pixel_size = tiled_plane.pixel_size
    y_range = tiled_plane.horizontal_range
    z_range = tiled_plane.vertical_range
    detector_size = tiled_plane.detector_size
    viewpoint = tiled_plane.viewpoint

    if viewpoint == "left":  # Invert coordinates
        y_range = -y_range[1], -y_range[0]
    xs_min = min(y_range) / pixel_size + 0.5 * detector_size[0]
    ys_min = min(z_range) / pixel_size + 0.5 * detector_size[1]

    tiling_histogram = HistogramAccumulator(bins=nbins)

    nlevel = len(datasets)

    _logger.debug(f"Process {tiled_plane.nb_images} images:")
    for index in range(tiled_plane.nb_images):
        image, x, y = wait_corrected_image(tiled_plane, index)
        _logger.debug(f"* Process image at position ({x}, {y})")
        _logger.debug("- Flat-field correction")
        if viewpoint != "left":
            image = numpy.flip(image, 1)
        else:
            x = -x

        offset = numpy.array(
            (
                int(round(y / pixel_size - ys_min)),
                int(round(x / pixel_size - xs_min)),
            )
        )
        _logger.debug(
            f"- Update level 0 at offset {tuple(offset)}, shape: {image.shape}"
        )
        copied_area = blit(image, datasets[0], offset)
        if copied_area is None:
            _logger.warning("! Image has no overlap: skipping")
            continue  # No more processing needed

        datasets[0].flush()  # Notify readers of the update

        if copied_area.shape != image.shape:
            _logger.warning(f"! Some pixels were not used: copied shape: {copied_area.shape}")

        _logger.debug("- Update current tiling histogram")
        tiling_histogram.update(image[copied_area.src_slices()])
        fill_histogram_nxdata(
            group["histogram"], tiling_histogram.bin_count, tiling_histogram.bin_edges
        )

        _logger.debug("- Update global histogram")
        histogram.update(image[copied_area.src_slices()])
        fill_histogram_nxdata(
            histogram_group, histogram.bin_count, histogram.bin_edges
        )

        # Update other levels of the pyramid of images
        for level in range(1, nlevel):
            # Get copy area with even bounds eventually enlarging it
            offset = numpy.asarray(copied_area.dst_offset)
            end = 2 * numpy.ceil((offset + copied_area.shape) / 2).astype(int)
            offset = 2 * (offset // 2)
            _logger.debug(
                f"- Binning level {level - 1} from {tuple(offset)} to {tuple(end)}"
            )
            binned_image = binning(
                datasets[level - 1][offset[0] : end[0], offset[1] : end[1]]
            )

            offset //= 2
            _logger.debug(
                f"- Update level {level} at offset {tuple(offset)}, shape: {binned_image.shape}"
            )
            copied_area = blit(binned_image, datasets[level], offset)
            datasets[level].flush()  # Notify readers of the update

            if copied_area is None:
                _logger.warning(
                    f"No overlap for level {level}, offset: {tuple(offset)}"
                )
                break


if __name__ == "__main__":
    import argparse

    logging.basicConfig(format='[%(asctime)s]%(levelname)s:%(name)s - %(message)s')

    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument(
        "input_filename", type=str, help="Input HDF5 file containing the tiling scan"
    )
    parser.add_argument("input_scan", type=str, help="HDF5 path of the tiling scan")
    parser.add_argument(
        "output_filename",
        type=str,
        default="output.h5",
        nargs="?",
        help="HDF5 file where to save the result",
    )
    parser.add_argument(
        "--verbose", "-v", action="count", default=0, help="Increase verbosity"
    )
    options = parser.parse_args()

    _logger.setLevel(
        {0: logging.WARNING, 1: logging.INFO, 2: logging.DEBUG}.get(
            options.verbose, logging.DEBUG
        )
    )

    nbins = 1000
    input_filename = str(pathlib.Path(options.input_filename).absolute())

    _logger.info(f"Write to {options.output_filename}")
    with h5py.File(options.output_filename, "w", libver="v110") as h5file:
        _logger.info(f"Read from {input_filename}::{options.input_scan}")

        wait_sequence_start(input_filename, options.input_scan)

        reader = wait_tiling_scan_reader(input_filename, options.input_scan)

        histogram = HistogramAccumulator(bins=nbins)

        # Prepare HDF5 file structure
        params = {}
        sides = "side", "front"

        _logger.info("Prepare HDF5 file for sequence")
        group = h5file.create_group("sequence")
        group["definition"] = "tiling_sequence"
        group.attrs["NX_class"] = "NXentry"
        group["tilings"] = sides
        histogram_group = prepare_histogram_nxdata(group, nbins)
        group.attrs["default"] = histogram_group.name
        group["acquisition_sequence"] = h5py.ExternalLink(
            input_filename, options.input_scan
        )
        group["scans"] = h5py.ExternalLink(input_filename, "/")

        for side in sides:
            _logger.info(f"Prepare HDF5 file for '{side}' tiling")
            group = h5file.create_group(side)
            datasets = prepare_tiling_dataset(
                group, reader.info, side, nbins, dtype=numpy.float16, fillvalue=numpy.nan
            )
            params[side] = dict(group=group, datasets=datasets, nbins=nbins)

        h5file.swmr_mode = True

        # Fill HDF5 file structure
        for side, kwargs in params.items():
            _logger.info(f"Process data for '{side}' tiling")
            tiled_plane = wait_tiled_plane(reader, side)
            fill_tiling_dataset(
                tiled_plane=tiled_plane,
                histogram=histogram,
                histogram_group=histogram_group,
                **kwargs
            )
        _logger.info("Done")
