import pytest
from pathlib import Path


def pytest_addoption(parser):
    parser.addoption("--testdata", action="store", default=None)


@pytest.fixture(scope="session")
def testdata_path(pytestconfig):
    testdata_path = pytestconfig.getoption("testdata")
    if testdata_path is None:
        pytest.skip("--testdata was not specified")
    testdata_path = Path(testdata_path).absolute()
    if not testdata_path.exists():
        raise ValueError(f"testdata path {testdata_path} does not exist")
    return testdata_path
