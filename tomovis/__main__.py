# coding: utf-8
# License: MIT (c) ESRF 2021
"""Prototype server for BM18 reconstruction/visualization"""
import argparse
from contextlib import contextmanager
import logging
import pathlib
import os
import tempfile
import socket
from typing import Optional
from blissdata.beacon.files import read_config

from flask_cors import CORS

from . import version
from .server import create_app
from .config import Configuration
from .utils.io_utils import set_soft_rlimit_nofile

logging.basicConfig(format="[%(asctime)s]%(levelname)s:%(name)s - %(message)s")

_logger = logging.getLogger(__name__)


def run(ip: str, port: int, base_dir: str, dev: bool) -> None:
    set_soft_rlimit_nofile()
    app = create_app(pathlib.Path(os.path.abspath(base_dir)))
    CORS(app)  # Enable cross-origin resource sharing: https://flask-cors.readthedocs.io
    app.run(port=port, host=ip, debug=dev)


@contextmanager
def basedir_context(basedir: Optional[str]):
    if basedir is not None:
        yield basedir
    else:
        with tempfile.TemporaryDirectory(prefix="tomovis") as tmpdir:
            _logger.warning("Using temporary folder to store files: %s", tmpdir)
            yield tmpdir


def main():
    # Parse command line args

    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument(
        "-p", "--port", type=int, default=8888, help="Port the server is listening on"
    )
    parser.add_argument(
        "--host",
        default=socket.gethostname(),
        help="Hostname/ip the server is listening on",
    )
    parser.add_argument(
        "--basedir", help="Base directory where to save/read HDF5 files"
    )
    parser.add_argument(
        "-v", "--verbose", action="count", default=0, help="Increase verbosity"
    )
    parser.add_argument("--dev", action="store_true", help="Use development mode")
    parser.add_argument("--autodelete", type=int, help="DEPRECATED")
    parser.add_argument(
        "--max-n-tilings",
        type=int,
        help="Number of kept tilings after which older tilings are automatically deleted",
    )
    parser.add_argument(
        "-c",
        "--config",
        type=str,
        default=None,
        help="File/resource which is used to setup the service (instead of the command line argument)",
    )

    options = parser.parse_args()

    tomovis_logger = logging.getLogger("tomovis")

    # Verbosity before reading the config file
    if options.verbose >= 1:
        tomovis_logger.setLevel(logging.INFO)

    if options.config:
        logging.info("Read config file: %s", options.config)
        config = read_config(options.config)
        for k, v in config.items():
            if k in dir(options):
                setattr(options, k, v)
            else:
                logging.warning(
                    "Item '%s' from config file is unsupported. Skipped.", k
                )

    if options.autodelete is not None:
        _logger.warning("--autodelete is deprecated, use --max-n-tilings")
    if options.max_n_tilings is None:
        options.max_n_tilings = (
            options.autodelete if options.autodelete is not None else 0
        )

    # Verbosity
    if options.verbose == 1:
        tomovis_logger.setLevel(logging.INFO)
    elif options.verbose == 2:
        logging.getLogger().setLevel(logging.INFO)
        tomovis_logger.setLevel(logging.DEBUG)
    elif options.verbose >= 3:
        logging.getLogger().setLevel(logging.DEBUG)

    tomovis_logger.info(f"tomovis version v{version}")

    # Flask app
    Configuration.MAX_N_TILINGS = options.max_n_tilings
    with basedir_context(options.basedir) as basedir:
        run(options.host, options.port, basedir, options.dev)


if __name__ == "__main__":
    main()
