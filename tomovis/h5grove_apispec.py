# codint: utf-8
# License: MIT (C) ESRF 2022
"""Add flask-apispec support to h5grove"""

import os
from typing import List, Optional

import flask
from flask_apispec import use_kwargs
from h5grove.flask_utils import get_content, make_encoded_response
from h5grove.content import DatasetContent, ResolvedEntityContent
from h5grove.models import LinkResolution
from h5grove.utils import parse_link_resolution_arg
import h5py
import hdf5plugin  # noqa
from marshmallow import fields, Schema, validate


class _H5GroveBaseSchema(Schema):
    file = fields.String(required=True)
    path = fields.String(load_default="/")


class _H5GroveSelectionSchema(_H5GroveBaseSchema):
    selection = fields.String(load_default=None)


class AttrSchema(_H5GroveBaseSchema):
    attr_keys = fields.List(fields.String, load_default=None)


class DataSchema(_H5GroveSelectionSchema):
    format = fields.String(
        validate=validate.OneOf(("json", "bin", "csv", "npy", "tiff")),
        load_default="json",
    )
    dtype = fields.String(
        validate=validate.OneOf(("origin", "safe")), load_default="origin"
    )
    flatten = fields.Boolean(load_default=False)


class MetaSchema(_H5GroveBaseSchema):
    resolve_links = fields.String(
        validate=validate.OneOf(
            ("true", "false") + tuple(e.value for e in LinkResolution)
        ),
        load_default=LinkResolution.ONLY_VALID.value,
    )


class StatsSchema(_H5GroveSelectionSchema):
    pass


def h5file_swmr(filename: str) -> h5py.File:
    """Open HDF5 file in single writter multiple reader mode"""
    return h5py.File(filename, mode="r", libver="v110", swmr=True)


def get_filename(file: str) -> str:
    """Return absolute file path from file query param"""
    full_file_path = os.path.join(flask.current_app.config["H5_BASE_DIR"], file)
    if not os.path.isfile(full_file_path):
        flask.abort(404, "File not found!")

    return full_file_path


@use_kwargs(AttrSchema, location="query")
def attr_route(file: str, path: str, attr_keys: Optional[List[str]]):
    """`/attr/` endpoint handler"""
    filename = get_filename(file)

    with h5file_swmr(filename) as h5file:
        content = get_content(h5file, path)
        assert isinstance(content, ResolvedEntityContent)
        return make_encoded_response(content.attributes(attr_keys))


@use_kwargs(DataSchema, location="query")
def data_route(
    file: str,
    path: str,
    format: str,
    dtype: str,
    flatten: bool,
    selection: Optional[str],
):
    """`/data/` endpoint handler"""
    filename = get_filename(file)
    with h5file_swmr(filename) as h5file:
        content = get_content(h5file, path)
        assert isinstance(content, DatasetContent)
        data = content.data(selection, flatten, dtype)
        return make_encoded_response(data, format)


@use_kwargs(MetaSchema, location="query")
def meta_route(file: str, path: str, resolve_links: str):
    """`/meta/` endpoint handler"""
    filename = get_filename(file)
    resolve_links = parse_link_resolution_arg(
        resolve_links,
        fallback=LinkResolution.ONLY_VALID,
    )

    with h5file_swmr(filename) as h5file:
        content = get_content(h5file, path, resolve_links)
        return make_encoded_response(content.metadata())


@use_kwargs(StatsSchema, location="query")
def stats_route(file: str, path: str, selection: Optional[str]):
    """`/stats/` endpoint handler"""
    filename = get_filename(file)
    with h5file_swmr(filename) as h5file:
        content = get_content(h5file, path)
        assert isinstance(content, DatasetContent)
        return make_encoded_response(content.data_stats(selection))


URL_RULES = {
    "/attr/": attr_route,
    "/data/": data_route,
    "/meta/": meta_route,
    "/stats/": stats_route,
}


BLUEPRINT = flask.Blueprint("h5grove", __name__)
for url, view_func in URL_RULES.items():
    BLUEPRINT.add_url_rule(url, view_func=view_func)
del url, view_func
