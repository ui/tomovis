from __future__ import annotations
import numpy
import logging
import pint
from scipy.fftpack import rfft


_logger = logging.getLogger(__name__)


def _freq_radio(sinos, ifrom, ito):
    size = (sinos.shape[0] + sinos.shape[0] % 2) // 2
    fs = numpy.empty((size, sinos.shape[1]))
    for i in range(ifrom, ito):
        line = sinos[:, i]
        f_signal = rfft(line)
        f = numpy.abs(f_signal[: (f_signal.size - 1) // 2 + 1])
        f2 = numpy.abs(f_signal[(f_signal.size - 1) // 2 + 1 :][::-1])
        if len(f) > len(f2):
            f[1:] += f2
        else:
            f[0:] += f2
        fs[:, i] = f
    with numpy.errstate(divide="ignore", invalid="ignore", under="ignore"):
        fs = numpy.log(fs)
    return fs


def gaussian(p, x):
    return p[3] + p[2] * numpy.exp(-((x - p[0]) ** 2) / (2 * p[1] ** 2))


def tukey(p, x):
    pos, std, alpha, height, background = p
    alpha = numpy.clip(alpha, 0, 1)
    pi = numpy.pi
    inv_alpha = 1 - alpha
    width = std / (1 - alpha * 0.5)
    xx = (numpy.abs(x - pos) - (width * 0.5 * inv_alpha)) / (width * 0.5 * alpha)
    xx = numpy.clip(xx, 0, 1)
    return (0.5 + numpy.cos(pi * xx) * 0.5) * height + background


def sinlet(p, x):
    std = p[1] * 2.5
    lin = numpy.maximum(0, std - numpy.abs(p[0] - x)) * 0.5 * numpy.pi / std
    return p[3] + p[2] * numpy.sin(lin)


def guess_cor_from_sample_stage(data):
    if data.detector_size is None:
        return None
    center = data.detector_size[0] / 2
    delta = (data.y_axis / data.sample_pixel_size).to("").magnitude
    return center + delta


class BlissSinoCor8:
    """
    Guess the COR from the sinogram symmetry.

    The algorithm based on each pixels of the sinogram.
    As result the angles don't have to be ordered.

    It will work for 360 deg projection or more.

    It uses guess for beamline metadata to pre select the location of the
    cor. If the beamline is not properly aligned, the algorithm will probably
    result in a wrong value.
    """

    def __init__(
        self,
        crop_around_hard_cor=True,
        hard_cor_std_um=100,
        hard_cor_weight=0.1,
        window=110,
        shift_sino=True,
        hard_cor_shape="tukey",
        hard_cor_alpha=0.5,
    ):
        self._crop_around_hard_cor = crop_around_hard_cor
        self._hard_cor_std_um = hard_cor_std_um
        self._hard_cor_weight = hard_cor_weight
        self._window = window
        self._shift_sino = shift_sino
        self._hard_cor_shape = hard_cor_shape
        self._hard_cor_alpha = hard_cor_alpha

    def __hash__(self):
        if self._hard_cor_shape == "tukey":
            cor_shape = (
                self._hard_cor_shape,
                self._hard_cor_std_um,
                self._hard_cor_alpha,
            )
        else:
            cor_shape = (self._hard_cor_shape, self._hard_cor_std_um)
        return hash(
            (
                self._use_paganin,
                self._crop_around_hard_cor,
                self._hard_cor_weight,
                self._window,
                cor_shape,
            )
        )

    def _px(self, detector_width, hard_cor, hard_cor_std):
        sym_range = None
        if hard_cor is not None:
            if self._crop_around_hard_cor:
                sym_range = int(hard_cor - hard_cor_std * 2), int(
                    hard_cor + hard_cor_std * 2
                )

        window = self._window
        if sym_range is not None:
            xx_from = max(window, sym_range[0])
            xx_to = max(xx_from, min(detector_width - window, sym_range[1]))
            if xx_from == xx_to:
                sym_range = None
        if sym_range is None:
            xx_from = window
            xx_to = detector_width - window
        xx = numpy.arange(xx_from, xx_to, 0.5)
        return xx

    def _symmetry_correlation(self, px, array, angles):
        window = self._window
        if self._shift_sino:
            shift_index = numpy.argmin(numpy.abs(angles - 180)) - numpy.argmin(
                numpy.abs(angles - 0)
            )
        else:
            shift_index = None
        px_from = int(px[0])
        px_to = int(numpy.ceil(px[-1]))
        f_coef = numpy.empty(len(px))
        f_array = _freq_radio(array, px_from - window, px_to + window)
        if shift_index is not None:
            shift_array = numpy.empty(array.shape, dtype=array.dtype)
            shift_array[0 : len(shift_array) - shift_index, :] = array[shift_index:, :]
            shift_array[len(shift_array) - shift_index :, :] = array[:shift_index, :]
            f_shift_array = _freq_radio(shift_array, px_from - window, px_to + window)
        else:
            f_shift_array = f_array

        for j, x in enumerate(px):
            i = int(numpy.floor(x))
            if x - i > 0.4:
                f_left = f_array[:, i - window : i]
                f_right = f_shift_array[:, i + 1 : i + window + 1][:, ::-1]
            else:
                f_left = f_array[:, i - window : i]
                f_right = f_shift_array[:, i : i + window][:, ::-1]
            with numpy.errstate(divide="ignore", invalid="ignore"):
                f_coef[j] = numpy.sum(numpy.abs(f_left - f_right))
        return f_coef

    def _hard_cor_correlation(self, px, hard_cor, hard_cor_std):
        if hard_cor is not None:
            signal = self._hard_cor_shape
            weight = self._hard_cor_weight
            alpha = self._hard_cor_alpha
            if signal == "sinlet":
                coef = sinlet((hard_cor, hard_cor_std, -weight, 1), px)
            elif signal == "gaussian":
                coef = gaussian((hard_cor, hard_cor_std, -weight, 1), px)
            elif signal == "tukey":
                coef = tukey((hard_cor, hard_cor_std * 2, alpha, -weight, 1), px)
            else:
                raise ValueError("Shape unsupported")
        else:
            coef = numpy.ones_like(px)
        return coef

    def _hard_cor(
        self,
        y_axis: pint.Quantity,
        sample_pixel_size: pint.Quantity,
        detector_width: int,
        detector_center_y: pint.Quantity | None,
    ) -> float | None:
        if y_axis is None or sample_pixel_size is None:
            return None
        if detector_center_y is None:
            detector_center_y = pint.Quantity(0, "mm")
        center = detector_width * 0.5
        delta = ((y_axis - detector_center_y) / sample_pixel_size).to("").magnitude
        return center + delta

    def find_cor(
        self,
        sinos: numpy.NdArray,
        angles: numpy.NdArray,
        y_axis: pint.Quantity,
        sample_pixel_size: pint.Quantity,
        detector_center_y: pint.Quantity | None = None,
    ) -> float | None:
        increment = numpy.abs(angles[0] - angles[1])
        if numpy.abs(angles[0] - angles[-1]) < 360 - 0.5 - increment:
            _logger.warning("Not enough angles, estimator skipped")
            return None

        detector_width = sinos.shape[1]

        hard_cor = self._hard_cor(
            y_axis=y_axis,
            sample_pixel_size=sample_pixel_size,
            detector_width=detector_width,
            detector_center_y=detector_center_y,
        )

        if hard_cor is None:
            logging.warning("No available metadata for hardware CoR")
        hard_cor_std = None
        if hard_cor is not None:
            hard_cor_std = self._hard_cor_std_um / sample_pixel_size.to("um").magnitude

        px = self._px(detector_width, hard_cor, hard_cor_std)

        coef_f = self._symmetry_correlation(
            px,
            sinos,
            angles,
        )
        coef_p = self._hard_cor_correlation(px, hard_cor, hard_cor_std)
        coef = coef_f * coef_p

        if len(px) > 0:
            cor = px[numpy.argmin(coef)]
        else:
            cor = None

        return cor
