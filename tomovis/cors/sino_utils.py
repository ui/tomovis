import numpy
import logging

_logger = logging.getLogger(__name__)


def _arg_nearest(array, value, threshold):
    """
    Find the index of the nearest value from an array, else -1
    """
    idx = (numpy.abs(array - value)).argmin()
    if numpy.abs(array[idx] - value) > threshold:
        return -1
    return idx


def normalize_sinogram(projections, angles, threshold=5):
    """
    Construct a sinogram from an unordered sinogram.

    Returns a sinogram (of n projections) ordered by angles, in which each
    projection have it opposite angle a the location `p+n/2%n`.

    Basically the first half part of the resulting sinogram is opposite to the
    second part.

    Projection without opposite angle from the input are just dropped.

    Arguments:
        projections: A list of 1d profile from projection
        angles: A list of angles (relative to this projections)
        threshold: A threshold to match angles

    Returns:
        The tuple containing the normalized sinogram and normalized angles
    """
    assert len(angles) == len(projections)
    if len(angles) == 0:
        return numpy.empty((0, 0)), angles
    # Common scan
    if numpy.isclose(angles[0], 0) and numpy.isclose(angles[-1], 360):
        angles = angles[:-1]
        projections = projections[:-1, :]
    # Common scan
    if numpy.isclose(angles[0], 360) and numpy.isclose(angles[-1], 0):
        angles = angles[1:]
        projections = projections[1:, :]

    angles = (angles - numpy.min(angles)) % 360
    sort_angles = numpy.argsort(angles)
    projections = projections[sort_angles]
    angles = angles[sort_angles]

    i_first_part = numpy.argwhere(angles < 180).flatten()
    i_second_part = numpy.fromiter(
        (_arg_nearest(angles, angles[i] + 180, 5) for i in i_first_part), numpy.int32
    )
    mask_threashold = i_second_part != -1
    i_first_part = i_first_part[mask_threashold]
    i_second_part = i_second_part[mask_threashold]
    norm_sino = numpy.concatenate(
        (projections[i_first_part.flatten()], projections[i_second_part.flatten()])
    )
    norm_angles = numpy.concatenate(
        (angles[i_first_part.flatten()], angles[i_second_part.flatten()])
    )
    assert len(norm_sino) % 2 == 0
    return norm_sino, norm_angles
