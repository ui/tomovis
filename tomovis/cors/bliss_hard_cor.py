from __future__ import annotations
import logging
import pint
import numpy

_logger = logging.getLogger(__name__)


class BlissHardCor:
    """
    Extract the CoR location from the hardware axis stored as metadata.

    If the beamine is properly calibrated, the y-axis at 0 is supposed to be
    centered to the detector.

    The precision/accuracy of the result is correlated to the precision/accuracy
    of the axis. Depending on the pixel size of the detector, it's usually about
    +/- 30 pixels of discrepancy even if the beamline was properly calibrated.
    """

    def find_cor(
        self,
        sinos: numpy.NdArray,
        y_axis: pint.Quantity,
        sample_pixel_size: pint.Quantity,
        detector_center_y: pint.Quantity | None = None,
    ) -> float | None:
        if y_axis is None or sample_pixel_size is None:
            return None
        if detector_center_y is None:
            detector_center_y = pint.Quantity(0, "mm")
        detector_width = sinos.shape[1]
        delta = ((y_axis - detector_center_y) / sample_pixel_size).to("").magnitude
        center = detector_width / 2
        return center + delta
