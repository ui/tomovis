import numpy
import logging


_logger = logging.getLogger(__name__)


class BlissSinoCor6:
    """
    Guess the COR from the sinogram symmetry.

    The angles have to be ordered. This is not checked by the algorythm.
    """

    def __init__(self, window):
        self._window = window

    def _compute_shifted_sym_coef(self, array, window, shift):
        """
        Compute a coef to find the symmetry.

        Lower is better.

        Arguments:
            window: The size of the window used to checl the symmetry.
            shift: Vertical shift which have to be applied to the right side
                   in order to compare it to the left side. It a shift of
                    180deg transformed into vertical shift on the sinogram.
        Returns:
            Tuple of x positions and the coef.
        """
        xx = numpy.arange(window, array.shape[1] - window, 0.5)
        coef = numpy.empty(len(xx))
        shift_array = numpy.empty(array.shape, dtype=array.dtype)
        shift_array[0 : len(shift_array) - shift, :] = array[shift:, :]
        shift_array[len(shift_array) - shift :, :] = array[:shift, :]

        for j, x in enumerate(xx):
            i = int(numpy.floor(x))
            if x - i > 0.4:
                left = array[:, i - window : i]
                right = shift_array[:, i + 1 : i + window + 1][:, ::-1]
            else:
                left = array[:, i - window : i]
                right = shift_array[:, i : i + window][:, ::-1]
            with numpy.errstate(divide="ignore", invalid="ignore"):
                coef[j] = numpy.sum(numpy.abs(left - right))
        return xx, coef

    def _find_sino_symmetry(self, sinos, angles, window):
        # reorder the angles to simplify the followng code
        if angles[1] - angles[0] < 0:
            angles = angles[::-1]
            sinos = sinos[::-1, :]

        # Guess there is a duplication of proj at 0 and 360
        # FIXME: This have to be improved
        if numpy.isclose(angles[0], 0) and numpy.isclose(angles[-1], 360):
            sinos = sinos[:-1, :]

        shift = numpy.argmin(numpy.abs(angles - 180)) - numpy.argmin(
            numpy.abs(angles - 0)
        )

        x, coef = self._compute_shifted_sym_coef(sinos, window=window, shift=shift)
        cor = x[numpy.argmin(coef)]
        if not numpy.isfinite(cor):
            return None
        return cor

    def find_cor(self, sinos, angles):
        if len(angles) == 0:
            _logger.warning("Not enough angles, estimator skipped")
            return None
        increment = numpy.abs(angles[0] - angles[1])
        if numpy.abs(angles[0] - angles[-1]) < 360 - 0.5 - increment:
            _logger.warning("Not enough angles, estimator skipped")
            return None
        cor = self._find_sino_symmetry(sinos, angles, window=self._window)
        return cor
