from __future__ import annotations
import numpy
import logging
import pint
from .sino_utils import normalize_sinogram

_logger = logging.getLogger(__name__)

try:
    from .nabu_sino_cor import SinoCORFinderHalfAcquisition
except ImportError:
    SinoCORFinderHalfAcquisition = None
    _logger.error("No available estimator for rotation position", exc_info=True)


def find_estimated_axis_position(
    projections: numpy.NdArray,
    angles: numpy.NdArray,
    backend: str,
    y_axis: pint.Quantity | None = None,
    sample_pixel_size: pint.Quantity | None = None,
    detector_center_y: pint.Quantity | None = None,
) -> float | None:
    """
    Find the estimated axis position.
    """
    if backend == "none":
        return None

    if backend == "nabu":
        if SinoCORFinderHalfAcquisition is None:
            _logger.error("No available estimator for axis position")
            return None
        try:
            sino, _ = normalize_sinogram(projections, angles)
            estimator = SinoCORFinderHalfAcquisition(sino)
            cor = estimator.find_cor()
        except Exception:
            _logger.error(
                "Error during the estimation of the axis position", exc_info=True
            )
            return None
        return cor

    if backend == "bliss-cor6":
        from .bliss_sino_cor6 import BlissSinoCor6

        estimator = BlissSinoCor6(window=110)
        try:
            sino, angles = normalize_sinogram(projections, angles)
            cor = estimator.find_cor(sino, angles)
        except Exception:
            _logger.error(
                "Error during the estimation of the axis position", exc_info=True
            )
            return None
        return cor

    if backend == "bliss-cor8":
        from .bliss_sino_cor8 import BlissSinoCor8

        estimator = BlissSinoCor8()
        try:
            cor = estimator.find_cor(
                projections,
                angles,
                y_axis=y_axis,
                sample_pixel_size=sample_pixel_size,
                detector_center_y=detector_center_y,
            )
        except Exception:
            _logger.error(
                "Error during the estimation of the axis position", exc_info=True
            )
            return None
        return cor

    if backend == "bliss-hardcor":
        from .bliss_hard_cor import BlissHardCor

        estimator = BlissHardCor()
        try:
            cor = estimator.find_cor(
                projections,
                y_axis=y_axis,
                sample_pixel_size=sample_pixel_size,
                detector_center_y=detector_center_y,
            )
        except Exception:
            _logger.error(
                "Error during the estimation of the axis position", exc_info=True
            )
            return None
        return cor

    raise ValueError(f"Unknown cor backend '{backend}'")
