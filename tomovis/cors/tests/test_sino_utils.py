import numpy
from .. import sino_utils


def test_empty():
    result, _ = sino_utils.normalize_sinogram([], [])
    assert result.size == 0


def test_already_normalized():
    angles = [1, 2, 181, 182]
    projs = numpy.array([[x] * 3 for x in angles])
    result, _ = sino_utils.normalize_sinogram(projs, angles)
    numpy.testing.assert_almost_equal(result, projs)


def test_unordered():
    angles = [1, 181, 2, 182]
    projs = numpy.array([[x] * 3 for x in angles])
    normalized_sino = numpy.array([[x] * 3 for x in sorted(angles)])
    result, _ = sino_utils.normalize_sinogram(projs, angles)
    numpy.testing.assert_almost_equal(result, normalized_sino)


def test_proj_without_opposite_angle():
    angles = [1, 181, 2, 182, 9]
    projs = numpy.array([[x] * 3 for x in angles])
    normalized_sino = numpy.array([[x] * 3 for x in sorted(angles[0:4])])
    result, _ = sino_utils.normalize_sinogram(projs, angles)
    numpy.testing.assert_almost_equal(result, normalized_sino)


def test_proj_without_opposite_angle__2nd_part():
    """The implementation split angles in 2 parts check that an angle in the second part can be rejected"""
    angles = [1, 181, 2, 182, 200]
    projs = numpy.array([[x] * 3 for x in angles])
    normalized_sino = numpy.array([[x] * 3 for x in sorted(angles[0:4])])
    result, _ = sino_utils.normalize_sinogram(projs, angles)
    numpy.testing.assert_almost_equal(result, normalized_sino)


def test_all_excluded():
    angles = [1, 10, 20, 30, 40]
    projs = numpy.array([[x] * 3 for x in angles])
    normalized_sino = numpy.empty((0, 3))
    result, _ = sino_utils.normalize_sinogram(projs, angles)
    numpy.testing.assert_almost_equal(result, normalized_sino)


def test_common_scan():
    angles = numpy.linspace(0, 360, 11)
    projs = numpy.array([[x] * 3 for x in angles])
    normalized_sino = numpy.array([[x] * 3 for x in sorted(angles[0:4])])
    norm_sino, norm_angles = sino_utils.normalize_sinogram(projs, angles)
    numpy.testing.assert_almost_equal(norm_angles, angles[:-1])
    numpy.testing.assert_almost_equal(norm_sino[:, 0], angles[:-1])


def test_common_reversed_scan():
    angles = numpy.linspace(360, 0, 11)
    expected_angles = numpy.sort(angles)[:-1]
    projs = numpy.array([[x] * 3 for x in angles])
    normalized_sino = numpy.array([[x] * 3 for x in sorted(angles[0:4])])
    norm_sino, norm_angles = sino_utils.normalize_sinogram(projs, angles)
    numpy.testing.assert_almost_equal(norm_angles, expected_angles)
    numpy.testing.assert_almost_equal(norm_sino[:, 0], expected_angles)
