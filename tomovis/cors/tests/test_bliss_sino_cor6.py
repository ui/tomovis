import pytest
import numpy
from ..bliss_sino_cor6 import BlissSinoCor6
from .utils import gen_sino


def test_cor_at_centered():
    estinator = BlissSinoCor6(window=100)

    angles = numpy.linspace(0, 360, 100)
    sinos = gen_sino(
        angles=angles,
        width=400,
        sample_width=100,
        sample_center=200,
        rotation_distance=100,
    )
    cor = estinator.find_cor(
        sinos=sinos,
        angles=angles,
    )
    # FIXME: The abs is really high, this have to be checked
    assert cor == pytest.approx(200, abs=4)


def test_cor_at_right():
    estinator = BlissSinoCor6(window=100)

    angles = numpy.linspace(0, 360, 100)
    sinos = gen_sino(
        angles=angles,
        width=400,
        sample_width=100,
        sample_center=300,
        rotation_distance=100,
    )
    cor = estinator.find_cor(
        sinos=sinos,
        angles=angles,
    )
    # FIXME: The abs is really high, this have to be checked
    assert cor == pytest.approx(300, abs=4)


def test_cor_at_right_reversed():
    estinator = BlissSinoCor6(window=100)

    angles = numpy.linspace(360, 0, 100)
    sinos = gen_sino(
        angles=angles,
        width=400,
        sample_width=100,
        sample_center=300,
        rotation_distance=100,
    )
    cor = estinator.find_cor(
        sinos=sinos,
        angles=angles,
    )
    # FIXME: The abs is really high, this have to be checked
    assert cor == pytest.approx(300, abs=4)


def test_cor_at_left():
    estinator = BlissSinoCor6(window=100)

    angles = numpy.linspace(0, 360, 100)
    sinos = gen_sino(
        angles=angles,
        width=400,
        sample_width=100,
        sample_center=100,
        rotation_distance=100,
    )
    cor = estinator.find_cor(
        sinos=sinos,
        angles=angles,
    )
    # FIXME: The abs is really high, this have to be checked
    assert cor == pytest.approx(100, abs=4)


def test_not_enough_angles():
    estinator = BlissSinoCor6(window=100)

    angles = numpy.linspace(0, 300, 100)
    sinos = gen_sino(
        angles=angles,
        width=400,
        sample_width=100,
        sample_center=100,
        rotation_distance=100,
    )
    cor = estinator.find_cor(
        sinos=sinos,
        angles=angles,
    )
    assert cor is None
