import pytest
import h5py
import numpy
import pint
from ..sino_cor import find_estimated_axis_position
from .utils import gen_sino
from tomo.helpers.technique_model_reader import TechniqueModelReader


@pytest.mark.parametrize(
    "params",
    [
        (
            "bliss-hardcor",
            "bliss_1.10-ebstomo_2.2__fullfield.h5",
            pytest.approx(8, abs=0.01),
        ),
    ],
)
def test_cor_on_resources(params, testdata_path):
    backend, acqscan, expected = params

    with h5py.File(testdata_path / acqscan) as h5:
        model = TechniqueModelReader(h5, sequence_path="1.1")
    q = model.y_axis
    y_axis = pint.Quantity(q[0], q[1])
    q = model.sample_pixel_size
    sample_pixel_size = pint.Quantity(q[0], q[1])

    angles = numpy.linspace(0, 360, 100)
    sinos = gen_sino(
        angles=angles,
        width=16,
        sample_width=100,
        sample_center=200,
        rotation_distance=100,
    )

    cor = find_estimated_axis_position(
        projections=sinos,
        angles=angles,
        backend=backend,
        y_axis=y_axis,
        sample_pixel_size=sample_pixel_size,
    )
    assert cor == expected


@pytest.mark.parametrize(
    "params",
    [
        ("none", None),
        ("nabu", pytest.approx(200, abs=66)),
        # FIXME: The abs is really high, this have to be checked
        ("bliss-cor6", pytest.approx(200, abs=4)),
    ],
)
def test_cor_at_centered(params):
    backend, expected = params
    angles = numpy.linspace(0, 360, 100)
    sinos = gen_sino(
        angles=angles,
        width=400,
        sample_width=100,
        sample_center=200,
        rotation_distance=100,
    )
    cor = find_estimated_axis_position(
        projections=sinos,
        angles=angles,
        backend=backend,
        y_axis=None,
        sample_pixel_size=None,
    )
    assert cor == expected


@pytest.mark.parametrize(
    "params",
    [
        ("none", None),
        ("nabu", pytest.approx(250, abs=55)),
        # FIXME: The abs is really high, this have to be checked
        ("bliss-cor6", pytest.approx(250, abs=4)),
    ],
)
def test_cor_at_right(params):
    backend, expected = params
    angles = numpy.linspace(0, 360, 100)
    sinos = gen_sino(
        angles=angles,
        width=400,
        sample_width=100,
        sample_center=250,
        rotation_distance=100,
    )
    cor = find_estimated_axis_position(
        projections=sinos,
        angles=angles,
        backend=backend,
        y_axis=None,
        sample_pixel_size=None,
    )
    assert cor == expected


@pytest.mark.parametrize(
    "params",
    [
        ("none", None),
        ("nabu", pytest.approx(250, abs=55)),
        # FIXME: The abs is really high, this have to be checked
        ("bliss-cor6", pytest.approx(250, abs=4)),
    ],
)
def test_cor_at_right_reversed(params):
    backend, expected = params
    angles = numpy.linspace(360, 0, 100)
    sinos = gen_sino(
        angles=angles,
        width=400,
        sample_width=100,
        sample_center=250,
        rotation_distance=100,
    )
    cor = find_estimated_axis_position(
        projections=sinos,
        angles=angles,
        backend=backend,
        y_axis=None,
        sample_pixel_size=None,
    )
    assert cor == expected


@pytest.mark.parametrize(
    "params",
    [
        ("none", None),
        ("nabu", pytest.approx(150, abs=100)),
        # FIXME: The abs is really high, this have to be checked
        ("bliss-cor6", pytest.approx(150, abs=4)),
    ],
)
def test_cor_at_left(params):
    backend, expected = params
    angles = numpy.linspace(0, 360, 100)
    sinos = gen_sino(
        angles=angles,
        width=400,
        sample_width=100,
        sample_center=150,
        rotation_distance=100,
    )
    cor = find_estimated_axis_position(
        projections=sinos,
        angles=angles,
        backend=backend,
        y_axis=None,
        sample_pixel_size=None,
    )
    assert cor == expected


def test_bliss_cor8_is_called(mocker):
    patched_estimator = mocker.patch("tomovis.cors.bliss_sino_cor8.BlissSinoCor8")
    patched_estimator.__call__().find_cor = mocker.Mock(return_value=10)

    projections = numpy.empty((3, 2))
    angles = numpy.linspace(0, 360, 3)
    y_axis = pint.Quantity(10, "mm")
    sample_pixel_size = pint.Quantity(10, "um")

    cor = find_estimated_axis_position(
        projections=projections,
        angles=angles,
        backend="bliss-cor8",
        y_axis=y_axis,
        sample_pixel_size=sample_pixel_size,
    )
    assert cor == 10

    patched_estimator.__call__().find_cor.assert_called_once_with(
        projections,
        angles,
        y_axis=y_axis,
        sample_pixel_size=sample_pixel_size,
        detector_center_y=None,
    )
