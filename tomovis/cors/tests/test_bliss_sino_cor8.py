import pytest
import h5py
import pint
from ..bliss_sino_cor8 import BlissSinoCor8


@pytest.mark.parametrize(
    "params",
    [
        ("b67ddd042cde48fe6006522e48561e448bc2688d_reduced.h5", "best_cor"),
        ("da296d7abaa1dbbd17f3233c8c0895a81147b4f6_reduced.h5", "best_cor"),
        ("e21eec4928ecf44f22ac82f2d38c2bb3b99ee8d6_reduced.h5", "best_cor"),
    ],
)
def test_datasets(params, testdata_path):
    estinator = BlissSinoCor8(window=int(110 / 20))
    data_filename, expected_cor = params

    def read_quantity(h5, key):
        d = h5[key]
        return pint.Quantity(d[()], d.attrs["units"])

    with h5py.File(testdata_path / data_filename, "r") as h5:
        angles = h5["angles"][...]
        corrected_sino = h5["corrected_sino"][...]
        y_axis = read_quantity(h5, "y_axis")
        sample_pixel_size = read_quantity(h5, "sample_pixel_size")
        if expected_cor == "best_cor":
            expected_cor = h5["best_cor"][()]

    cor = estinator.find_cor(
        sinos=corrected_sino,
        angles=angles,
        y_axis=y_axis,
        sample_pixel_size=sample_pixel_size,
    )
    assert cor == pytest.approx(expected_cor, abs=4)
