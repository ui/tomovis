import pytest
import numpy
import pint
from ..bliss_hard_cor import BlissHardCor


def test_bliss_hard_cor():
    y_axis = pint.Quantity(-0.02, "mm")
    sample_pixel_size = pint.Quantity(10, "um")
    sinos = numpy.empty((10, 20))
    estimator = BlissHardCor()
    assert estimator.find_cor(
        sinos, y_axis, sample_pixel_size, detector_center_y=None
    ) == pytest.approx(8.0, abs=0.01)


def test_bliss_hard_cor_with_detector_center_zero():
    y_axis = pint.Quantity(-0.02, "mm")
    sample_pixel_size = pint.Quantity(10, "um")
    detector_center_y = pint.Quantity(0, "mm")
    sinos = numpy.empty((10, 20))
    estimator = BlissHardCor()
    assert estimator.find_cor(
        sinos, y_axis, sample_pixel_size, detector_center_y=detector_center_y
    ) == pytest.approx(8.0, abs=0.01)


def test_bliss_hard_cor_with_detector_center():
    y_axis = pint.Quantity(0.00, "mm")
    sample_pixel_size = pint.Quantity(10, "um")
    detector_center_y = pint.Quantity(0.02, "mm")
    sinos = numpy.empty((10, 20))
    estimator = BlissHardCor()
    assert estimator.find_cor(
        sinos, y_axis, sample_pixel_size, detector_center_y=detector_center_y
    ) == pytest.approx(8.0, abs=0.01)
