import numpy


def gen_sino(angles, width, sample_width, sample_center, rotation_distance):
    sinos = numpy.empty((len(numpy.deg2rad(angles)), width))
    pixels = numpy.arange(0, sinos.shape[1])
    zeros = numpy.zeros_like(pixels)
    for i, a in enumerate(angles):
        sinos[i] = numpy.minimum(
            zeros,
            (sample_width / 2)
            - numpy.abs(pixels - sample_center - numpy.sin(a) * rotation_distance),
        )
    return sinos
