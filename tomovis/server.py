# coding: utf-8
# License: MIT (c) ESRF 2021
"""Prototype server for BM18 reconstruction/visualization"""
import pathlib

import flask
from flask_apispec import FlaskApiSpec

# from flask_cors import CORS

from .crud_handlers import CrudBlueprint as _CrudBlueprint
from .nabu_fullfield import NabuFullfieldBlueprint
from .image_tiling import ImageTilingBlueprint
from . import h5grove_apispec
from . import resources
from . import version


def status():
    """View function providing status information"""
    return dict(
        name=flask.current_app.name,
        version=version,
        links=[
            dict(
                rel="openapi",
                action="GET",
                href=f"{flask.request.root_url}{flask.current_app.config['APISPEC_SWAGGER_URL'].lstrip('/')}",
            ),
        ],
    )


def create_app(base_dir: pathlib.Path, name: str = __name__):
    """Create Flask app"""
    app = flask.Flask(name, static_folder=resources.STATIC_PATH)

    app.config["H5_BASE_DIR"] = str(base_dir)
    app.register_blueprint(h5grove_apispec.BLUEPRINT, url_prefix="/h5grove/")
    app.register_blueprint(
        NabuFullfieldBlueprint("nabu_fullfield", name, base_dir=base_dir),
        url_prefix="/nabu_fullfield",
    )
    app.register_blueprint(
        ImageTilingBlueprint("image_tiling", name, base_dir=base_dir),
        url_prefix="/image_tiling",
    )
    app.add_url_rule("/status", view_func=status)

    def h5web():
        return flask.send_file(resources.RESOURCES_PATH / "h5web.html")

    app.add_url_rule("/h5web", view_func=h5web)

    # apispec
    app.config["APISPEC_SWAGGER_URL"] = "/openapi.json"
    app.config["APISPEC_SWAGGER_UI_URL"] = None
    app.config["APISPEC_TITLE"] = __name__
    app.config["APISPEC_VERSION"] = version
    app.config["APISPEC_OAS_VERSION"] = "2.0"
    docs = FlaskApiSpec(app)

    for name, blueprint in app.blueprints.items():
        if isinstance(blueprint, _CrudBlueprint):
            for resource in blueprint.resources.values():
                docs.register(resource, blueprint=name)

    docs.register(h5web)
    for view in h5grove_apispec.URL_RULES.values():
        docs.register(view, blueprint="h5grove")

    app._apispec = docs

    return app
