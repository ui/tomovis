# coding: utf-8
# License: MIT (c) ESRF 2022
"""Client functions for tomovis"""
from typing import Optional

import requests


def create_tiling(
    base_url: str,
    h5file: str,
    scan_h5entry: str,
    timeout: Optional[float] = None,
) -> Optional[str]:
    config = dict(
        dataset=dict(
            location=h5file,
            hdf5_entry=scan_h5entry,
        ),
    )

    try:
        response = requests.post(
            f"{base_url}/image_tiling/",
            json=config,
            timeout=timeout,
        )
    except requests.Timeout:
        return None

    if response.status_code != 202:
        return None
    content = response.json()
    return content["id"]


def delete_tiling(base_url: str, id_: str, timeout: Optional[float] = None) -> bool:
    try:
        response = requests.delete(f"{base_url}/image_tiling/{id_}", timeout=timeout)
    except requests.Timeout:
        return False

    return response.status_code == 204


def clear_tiling(base_url: str, timeout: Optional[float] = None) -> bool:
    try:
        response = requests.get(f"{base_url}/image_tiling/", timeout=timeout)
    except requests.Timeout:
        return False

    if response.status_code != 200:
        return False
    content = response.json()

    results = [delete_tiling(base_url, desc["id"]) for desc in content]
    return all(results)
