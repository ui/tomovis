# coding: utf-8
# License: MIT (c) ESRF 2021
"""Shared schemas"""
from marshmallow import Schema, fields, validate


class BaseConfigSchema(Schema):
    pass


class BaseResourceSchema(Schema):
    id = fields.String(required=True)
    status = fields.String(
        required=True,
        validate=validate.OneOf(
            ("created", "pending", "processing", "cancelled", "done")
        ),
    )
    config = fields.Nested(BaseConfigSchema, required=True)


class Hdf5EntrySchema(Schema):

    location = fields.String(required=True)
    hdf5_entry = fields.String(required=True)


class Link(Schema):
    """URL to related resource"""

    rel = fields.String(validate=validate.OneOf(("data",)))  # TODO more fields?
    href = fields.Url(required=True)
    action = fields.String(
        load_default="GET",
        dump_default="GET",
        validate=validate.OneOf(("DELETE", "GET", "POST")),
    )


class Calibration(Schema):
    """Reconstructed volume information"""

    origin = fields.List(
        fields.Float,
        load_default=(0, 0, 0),
        dump_default=(0, 0, 0),
        validate=validate.Length(equal=3),
    )
    scale = fields.List(
        fields.Float(validate=validate.Range(min=0, min_inclusive=False)),
        load_default=(1, 1, 1),
        dump_default=(1, 1, 1),
        validate=validate.Length(equal=3),
    )


# Nabu fullfield


class NabuDatasetSchema(Schema):
    """See https://www.silx.org/pub/nabu/doc/nabu_config_items.html#dataset"""

    location = fields.String(required=True)
    hdf5_entry = fields.String()
    binning = fields.Integer(validate=validate.Range(min=1))
    binning_z = fields.Integer(validate=validate.Range(min=1))
    projections_subsampling = fields.Integer(validate=validate.Range(min=1))


class NabuPreprocSchema(Schema):
    flatfield = fields.Boolean()
    ccd_filter_enabled = fields.Boolean()
    ccd_filter_threshold = fields.Float()
    double_flatfield_enabled = fields.Boolean()
    take_logarithm = fields.Boolean()


class NabuPhaseSchema(Schema):
    """See https://www.silx.org/pub/nabu/doc/nabu_config_items.html#phase"""

    method = fields.String(validate=validate.OneOf(("none", "paganin")))
    delta_beta = fields.Float(validate=validate.Range(min=0, min_inclusive=False))
    unsharp_coeff = fields.Float(validate=validate.Range(min=0))
    unsharp_sigma = fields.Float(validate=validate.Range(min=0))


class NabuReconstructionSchema(Schema):
    """See https://www.silx.org/pub/nabu/doc/nabu_config_items.html#reconstruction"""

    method = fields.String(validate=validate.OneOf(("FBP", "none")))
    rotation_axis_position = fields.String(
        validate=validate.OneOf(
            (
                "",
                "centered",
                "global",
                "sliding-window",
                "growing-window",
                "sino-coarse-to-fine",
            )
        )
    )
    padding_type = fields.String(validate=validate.OneOf(("zeros", "edges")))
    enable_halftomo = fields.Boolean()
    start_x = fields.Integer()
    end_x = fields.Integer()
    start_y = fields.Integer()
    end_y = fields.Integer()
    start_z = fields.Integer()
    end_z = fields.Integer()


class NabuPostProcSchema(Schema):
    """See https://www.silx.org/pub/nabu/doc/nabu_config_items.html#postproc"""

    output_histogram = fields.Boolean()
    histogram_bins = fields.Integer(validate=validate.Range(min=0))


class NabuFullfieldConfigSchema(BaseConfigSchema):
    """See https://www.silx.org/pub/nabu/doc/nabu_config_items.html"""

    dataset = fields.Nested(NabuDatasetSchema, required=True)
    preproc = fields.Nested(NabuPreprocSchema)
    phase = fields.Nested(NabuPhaseSchema)
    reconstruction = fields.Nested(NabuReconstructionSchema)
    postproc = fields.Nested(NabuPostProcSchema)


class NabuFullfieldSchema(BaseResourceSchema):
    """nabu fullfield reconstruction parameters and related resources"""

    config = fields.Nested(NabuFullfieldConfigSchema, required=True)
    calibration = fields.Nested(Calibration())
    links = fields.List(fields.Nested(Link))


# Image tiling


class ImageTilingConfigSchema(BaseConfigSchema):
    """Image tiling parameters"""

    dataset = fields.Nested(Hdf5EntrySchema, required=True)


class ImageTilingSchema(BaseResourceSchema):
    """Image tiling parameters and related resources"""

    config = fields.Nested(ImageTilingConfigSchema, required=True)
    output = fields.List(fields.Nested(Hdf5EntrySchema), required=True)
    h5grove_url = fields.Url(required=True)
    links = fields.List(fields.Nested(Link))
