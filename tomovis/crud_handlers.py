# coding: utf-8
# License: MIT (c) ESRF 2021
"""Helpers for CRUD REST API"""
from collections.abc import MutableMapping
from datetime import datetime
import functools
import json
import logging
import pathlib
from typing import Dict, Iterator, List, Optional, Type
import shutil
import uuid

import flask
from flask_apispec import marshal_with, MethodResource, use_kwargs
import flask_restful
from marshmallow import Schema, ValidationError

from .subprocess_executor import CancellableFuture, SubprocessExecutor
from .utils.reentrant_lock import ReentrantLock


_logger = logging.getLogger(__name__)


class FolderBasedResourceManager(MutableMapping):
    _DATETIME_FORMAT = "%Y-%m-%dT%Hh%Mm%S.%f"

    def __init__(self, base_path: pathlib.Path):
        super().__init__()
        self.__base_path = base_path
        self.__lock = ReentrantLock(str(self.__base_path / "lock"))

    def _ensure_base_path(self) -> pathlib.Path:
        if not self.__base_path.is_dir():
            self.__base_path.mkdir(parents=True)
        return self.__base_path

    @property
    def lock(self):
        self._ensure_base_path()
        return self.__lock

    def dir_path(self, id_: str) -> pathlib.Path:
        return self._ensure_base_path() / id_

    def config_path(self, id_: str) -> pathlib.Path:
        return self.dir_path(id_) / "config.json"

    def status_path(self, id_: str) -> pathlib.Path:
        return self.dir_path(id_) / "status.txt"

    def get_status(self, id_: str) -> str:
        with self.lock:
            return self.status_path(id_).read_text()

    def set_status(self, id_: str, status: str):
        with self.lock:
            self.status_path(id_).write_text(status)

    def create(self, config: dict) -> str:
        create_time = datetime.now()
        id_ = f"{create_time.strftime(self._DATETIME_FORMAT)}-{uuid.uuid1().hex}"
        _logger.info(f"Creating resource {id_}: {config}")
        if id_ in self:
            _logger.critical(f"Creating {id_} which already exists")
        self[id_] = config
        return id_

    def __setitem__(self, id_: str, config: dict) -> None:
        with self.lock:
            self.dir_path(id_).mkdir(exist_ok=True)
            self.set_status(id_, "created")
            self.config_path(id_).write_text(json.dumps(config))

    def __delitem__(self, id_: str) -> None:
        with self.lock:
            shutil.rmtree(str(self.dir_path(id_)))

    def __getitem__(self, id_: str) -> dict:
        with self.lock:
            try:
                status = self.get_status(id_)
                config = self.config_path(id_).read_text()
            except FileNotFoundError:
                raise KeyError(f"Resource {id_} not available")
        create_time = datetime.strptime(id_.rsplit("-", 1)[0], self._DATETIME_FORMAT)
        return dict(
            id=id_,
            create_time=create_time.isoformat(),
            status=status,
            config=json.loads(config),
        )

    def __iter__(self) -> Iterator[str]:
        with self.lock:
            base_path_content = list(self._ensure_base_path().iterdir())
        for subpath in sorted(base_path_content):
            if subpath.is_dir():
                yield subpath.name

    def __len__(self) -> int:
        with self.lock:
            return len(self._ensure_base_path().iterdir())


class SubprocessExecutorManager(FolderBasedResourceManager):
    def __init__(self, base_path: pathlib.Path):
        super().__init__(base_path)
        self.__executor = SubprocessExecutor()
        self.__futures = {}

        # Restore futures, restarting unfinished tasks
        for id_ in self:
            status = self.get_status(id_)
            _logger.info(f"Reloading {id_} with status {status}")
            if status != "done":
                # Try to restart the task
                try:
                    config_text = self.config_path(id_).read_text()
                except FileNotFoundError:
                    _logger.warning(f"Cannot load config file for {id_}")
                    config = None
                else:
                    try:
                        config = json.loads(config_text)
                    except json.JSONDecodeError:
                        _logger.error(f"Cannot parse config file for {id_}")
                        config = None

                if config is not None:
                    self[id_] = config
                    _logger.info(
                        f"Reloaded {id_}: Added to executor, status {self.get_status(id_)}"
                    )
                    continue

            future = CancellableFuture()
            if status != "done":
                future.cancel()
            if future.set_running_or_notify_cancel():
                future.set_result(None)
            self.__set_status_from_future(id_, future)
            self.__futures[id_] = future
            _logger.info(f"Reloaded {id_}: Status {self.get_status(id_)}")

    def __set_status_from_future(self, id_: str, future: CancellableFuture):
        if future.running():
            status = "running"
        elif future.cancelled():
            status = "cancelled"
        elif future.done():
            status = "done"
        else:
            status = "pending"
        self.set_status(id_, status)

    @property
    def max_n_resources(self) -> int:
        """Maximum number of resources after which older ones are deleted.

        Values <=0 disable this autodelete feature.

        Override in subclass.
        """
        return 0

    def __handle_autodelete(self):
        if self.max_n_resources <= 0:
            return  # No autodelete

        count = 0
        with self.lock:
            for id_ in sorted(self, reverse=True):
                if count >= self.max_n_resources:
                    _logger.info(
                        f"Autodelete {self.dir_path(id_)}, to much resources: {count}>={self.max_n_resources}"
                    )
                    del self[id_]
                else:
                    count += 1

    def command(self, id_: str, config: dict) -> List[str]:
        raise NotImplementedError()

    def __setitem__(self, id_: str, config: dict) -> None:
        super().__setitem__(id_, config)
        future = self.__executor.submit(self.command(id_, config))
        self.__futures[id_] = future
        self.__set_status_from_future(id_, future)
        set_status_from_future = functools.partial(self.__set_status_from_future, id_)
        future.add_done_callback(set_status_from_future)
        future.add_running_callback(set_status_from_future)
        self.__handle_autodelete()

    def __delitem__(self, id_: str) -> None:
        future = self.__futures.pop(id_, None)
        if future is not None:
            future.cancel()
        super().__delitem__(id_)


class _Resource(MethodResource, flask_restful.Resource):
    """End point to read, update and delete a resource"""

    @staticmethod
    def factory(resource_schema: Type[Schema]):
        class Resource(_Resource):
            @marshal_with(resource_schema())
            def get(self, resource_id: str):
                return super().get(resource_id)

        return Resource

    def __init__(self, handler: Optional[MutableMapping] = None):
        flask_restful.Resource.__init__(self)
        self.__handler = handler

    def _abort_if_doesnt_exist(self, resource_id: str):
        if resource_id not in self.__handler:
            flask_restful.abort(404, message=f"Resource {resource_id} doesn't exist")

    def get(self, resource_id: str):
        self._abort_if_doesnt_exist(resource_id)
        return self.__handler[resource_id]

    @marshal_with(Schema, code=204)
    def delete(self, resource_id: str):
        self._abort_if_doesnt_exist(resource_id)
        try:
            del self.__handler[resource_id]
        except Exception as e:
            return e.args, 500  # Internal Server Error
        return None, 204


class _ResourcesList(MethodResource, flask_restful.Resource):
    """End point to create a resource and get the list"""

    @staticmethod
    def factory(create_schema: Type[Schema], resource_schema: Type[Schema]):
        class ResourcesList(_ResourcesList):
            @use_kwargs(create_schema())
            @marshal_with(resource_schema())
            def post(self, **kwargs):
                return super().post(**kwargs)

            @marshal_with(resource_schema(many=True))
            def get(self):
                return super().get()

        return ResourcesList

    def __init__(self, handler: Optional[MutableMapping] = None):
        flask_restful.Resource.__init__(self)
        self.__handler = handler

    def get(self):  # TODO paging/filter by status/return summary only?
        return dict(items=list(self.__handler.values()))

    def post(self, **kwargs):
        try:
            id_ = self.__handler.create(kwargs)
        except ValidationError as e:
            return e.messages, 400  # Bad Request
        except Exception as e:
            return e.args, 500  # Internal server Error
        return self.__handler[id_], 202  # Accepted (or 201 Created)


class CrudBlueprint(flask.Blueprint):
    """Blueprint for CRUD REST API"""

    def __init__(
        self,
        manager: MutableMapping,
        create_schema: Type[Schema],
        resource_schema: Type[Schema],
        *args,
        **kwargs,
    ):
        super().__init__(*args, **kwargs)

        self.__resources = {
            "/": _ResourcesList.factory(create_schema, resource_schema),
            "/<resource_id>": _Resource.factory(resource_schema),
        }

        self.__api = flask_restful.Api(self)
        for endpoint, resourceClass in self.__resources.items():
            self.__api.add_resource(
                resourceClass,
                endpoint,
                resource_class_args=(manager,),
            )

    @property
    def resources(self) -> Dict[str, flask_restful.Resource]:
        """Mapping of endpoint to Resource class"""
        return self.__resources.copy()

    @property
    def api(self):
        """Corresponding flask_restful.Api"""
        return self.__api
