# coding: utf-8
# License: MIT (c) ESRF 2021
"""Image tiling CRUD REST API"""
import logging
import pathlib
import sys
from typing import List, Optional

import flask

from .config import Configuration
from .crud_handlers import CrudBlueprint, SubprocessExecutorManager
from .schemas import ImageTilingConfigSchema, ImageTilingSchema

_logger = logging.getLogger(__name__)


class ProcessManager(SubprocessExecutorManager):
    def __init__(self, base_path: pathlib.Path):
        _logger.info("Start image_tiling execution manager")
        super().__init__(base_path)

    @property
    def max_n_resources(self) -> int:
        return Configuration.MAX_N_TILINGS

    def command(self, id_: str, config: dict) -> List[str]:
        if _logger.getEffectiveLevel() <= logging.DEBUG:
            verbosity = 2
        elif _logger.getEffectiveLevel() == logging.INFO:
            verbosity = 1
        else:
            verbosity = 0

        return [
            sys.executable,
            "-m",
            "tomovis.process.image_tiling",
            config["dataset"]["location"],
            config["dataset"]["hdf5_entry"],
            str(self.dir_path(id_) / "output.h5"),
        ] + ["-v"] * verbosity

    def __getitem__(self, id_: str) -> dict:
        hdf5_filename = f"image_tiling/{id_}/output.h5"

        content = super().__getitem__(id_)
        content.update(
            dict(
                output=[
                    dict(
                        location=hdf5_filename,
                        hdf5_entry="/front/",
                    ),
                    dict(
                        location=hdf5_filename,
                        hdf5_entry="/side/",
                    ),
                ],
                h5grove_url=f"{flask.request.root_url}h5grove/",
                links=[
                    dict(
                        rel="h5web",
                        action="GET",
                        href=f"{flask.request.root_url}h5web?filepath={hdf5_filename}",
                    ),
                ],
            )
        )
        return content


class ImageTilingBlueprint(CrudBlueprint):
    """Flask blueprint for image tiling end point"""

    def __init__(self, *args, base_dir: Optional[pathlib.Path] = None, **kwargs):
        assert base_dir is not None

        # Create subdirectory to store reconstructions
        path = base_dir / "image_tiling"
        path.mkdir(parents=True, exist_ok=True)

        manager = ProcessManager(path)
        super().__init__(
            manager, ImageTilingConfigSchema, ImageTilingSchema, *args, **kwargs
        )
