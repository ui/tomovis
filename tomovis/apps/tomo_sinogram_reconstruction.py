#!/usr/bin/env python
# coding: utf-8
# License: MIT (c) ESRF 2023
"""
Helper to process a slice reconstruction from a BLISS tomo scan containing a
sinogram.

This will produce a reconstruct.h5 file as output.
"""
import logging
import argparse
import os
from ..ewoks import tomo_sinogram_reconstruction


logging.basicConfig(
    level=logging.INFO,
    format='[%(asctime)s]%(levelname)s:%(name)s - %(message)s',
)


def patch_sidecar_lib():
    tomo_sinogram_reconstruction.add_autoproc_attachment = lambda *args, **kwargs: 1
    tomo_sinogram_reconstruction.notify = lambda *args, **kwargs: None


def main():
    patch_sidecar_lib()

    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument("scan_filename", help="Path to a BLISS scan file")
    parser.add_argument(
        "--axisposition",
        default=None,
        type=float,
        help="Use a specific axis position (in pixel)",
    )
    parser.add_argument(
        "--deltabeta",
        default=None,
        type=float,
        help="Use a delta/beta factor [default is 200]",
    )
    parser.add_argument(
        "--backends",
        default=None,
        type=str,
        help="List of backends [default is 'nabu,silx']",
    )
    options = parser.parse_args()
    rawDirectory, fileTemplate = os.path.split(options.scan_filename)
    taskDirectory = os.path.abspath(os.getcwd())

    inputs = {
        "metadata": {
            "workingDirectory": taskDirectory,
            "imageDirectory": rawDirectory,
            "fileTemplate": fileTemplate,
            "beamLineName": "id00",
        },
        "autoProcProgramId": 1,
        "dataCollectionId": 2,
        "taskDirectory": taskDirectory,
    }
    if options.axisposition is not None:
        inputs["axisposition"] = options.axisposition
    if options.deltabeta is not None:
        inputs["deltabeta"] = options.deltabeta
    if options.backends is not None:
        inputs["backends"] = options.backends

    task = tomo_sinogram_reconstruction.TomoSinogramReconstruction(inputs=inputs)
    task.run()
