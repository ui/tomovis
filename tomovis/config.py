# coding: utf-8
# License: MIT (c) ESRF 2022


class Configuration:
    """Holds config values"""

    MAX_N_TILINGS = 0
    """Maximum number of tilings to keep before deleting older ones.

    Set to 0 or negative to disable it.
    """
