import numpy
import h5py
from .. import tomo_sinogram_reconstruction


def test_mocked_file(mocker, tmp_path):
    add_autoproc_attachment = mocker.patch.object(
        tomo_sinogram_reconstruction, "add_autoproc_attachment"
    )
    notify = mocker.patch.object(tomo_sinogram_reconstruction, "notify")

    taskDirectory = str(tmp_path / "precessing")
    (tmp_path / "raw").mkdir()
    workingDirectory = str(tmp_path / "raw")
    with h5py.File(f"{workingDirectory}/scans.h5", "w") as h5:
        h5.attrs["NX_class"] = "NXroot"
        h5.attrs["creator"] = "Bliss"
        scan1 = h5.create_group("1.1")
        scan1.attrs["NX_class"] = "NXentry"
        scan1["measurement/rotation"] = [1, 1, 1, 2, 2, 2]
        scan1["measurement/sinogram"] = numpy.arange(6)
        scan1["measurement/translation"] = [1, 2, 3, 1, 2, 3]
        # FIXME: See https://gitlab.esrf.fr/tomo/ebs-tomo/-/issues/71
        proj = numpy.arange(6)
        proj.shape = 2, 3
        scan1["measurement/proj_spectrum"] = proj

        scan1["start_time"] = "2023-01-20T12:10:52.827871+01:00"
        scan1["end_time"] = "2023-01-20T12:10:52.827871+01:00"

        scan1["instrument/positioners/foo"] = numpy.arange(2)
        scan1["instrument/positioners/foo"].attrs["units"] = "mm"

        scan1["technique/optic/sample_pixel_size"] = 1
        scan1["technique/optic/sample_pixel_size"].attrs["units"] = "um"
        scan1["technique/tomoconfig/translation_x"] = ["foo"]
        scan1["technique/tomoconfig/translation_y"] = ["foo"]
        scan1["technique/tomoconfig/sample_x"] = ["foo"]
        scan1["technique/tomoconfig/sample_y"] = ["foo"]
        scan1["technique/scan/sample_detector_distance"] = 100
        scan1["technique/scan/sample_detector_distance"].attrs["units"] = "mm"
        scan1["technique/scan/energy"] = 100
        scan1["technique/scan/energy"].attrs["units"] = "keV"

    inputs = {
        "metadata": {
            "workingDirectory": workingDirectory,
            "imageDirectory": workingDirectory,
            "fileTemplate": "scans.h5",
            "beamLineName": "id00",
        },
        "autoProcProgramId": 1,
        "dataCollectionId": 2,
        "taskDirectory": taskDirectory,
        "backends": "dummy",
        "cor_backend": "none",
    }
    task = tomo_sinogram_reconstruction.TomoSinogramReconstruction(inputs=inputs)
    task.run()

    add_autoproc_attachment.assert_called_once()
    notify.assert_called_once()
