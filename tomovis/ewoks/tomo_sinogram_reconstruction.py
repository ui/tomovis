from __future__ import annotations
import logging
import typing
import contextlib
import time
import os
import h5py
import numpy
import sys
import pint

from ewokscore import Task

_logger = logging.getLogger(__name__)

TEST_CONTEXT = "pytest" in sys.argv[0]
APP_CONTEXT = "tomovis-" in sys.argv[0]

try:
    from sidecar.ispyb.autoproc import add_autoproc_attachment
    from sidecar.kombu.notify import notify
except Exception:
    # FIXME: https://gitlab.esrf.fr/workflow/sidecar/-/issues/25
    def add_autoproc_attachment(*args, **kwargs):
        return None

    def notify(*args, **kwargs):
        pass

    if not TEST_CONTEXT and not APP_CONTEXT:
        raise

from tomovis.utils.retry_utils import retry_on_error
from silx.io import h5py_utils

try:
    from silx.image import backprojection
except:
    _logger.error("Error during import of backprojection from silx", exc_info=True)
    backprojection = None


from tomo.helpers.sinogram_reader import SinogramReader
from tomo.helpers.technique_model_reader import TechniqueModelReader
from ..cors.sino_cor import find_estimated_axis_position
from silx.math.combo import min_max

try:
    import nabu
    from nabu.reconstruction.fbp import Backprojector
    from nabu.preproc.phase import PaganinPhaseRetrieval
    from nabu.preproc.ccd import Log
except ImportError:
    _logger.error("Error during import of nabu", exc_info=True)
    nabu = None


try:
    from blissdata.beacon.files import read_config
except ImportError:
    read_config = None


TOMOVIS_CONFIG = None


def read_tomovis_config():
    global TOMOVIS_CONFIG
    if TOMOVIS_CONFIG is not None:
        return TOMOVIS_CONFIG
    if read_config is None:
        return {}
    if "BEACON_HOST" not in os.environ:
        return {}
    filename = "beacon:///services/tomovis.yml"
    config = read_config(filename)
    TOMOVIS_CONFIG = config
    return config


DEFAULT_BACKENDS = ["nabu", "silx", "dummy"]
DEFAULT_COR_BACKEND = "nabu"


@contextlib.contextmanager
def timeit():
    class Duration:
        def __init__(self):
            self._value = None

        @property
        def duration(self):
            return self._value

    t0 = time.time()
    obj = Duration()
    try:
        yield obj
    finally:
        obj._value = time.time() - t0


class ScanData(typing.NamedTuple):
    sino: numpy.ndarray
    model: object
    pixel_size: float
    distance: float
    energy: float
    sample_pixel_size: pint.Quantity
    y_axis: pint.Quantity
    detector_center_y: pint.Quantity


@h5py_utils.retry(retry_timeout=120, retry_period=2, retry_on_error=retry_on_error)
def read_scan_data(filename) -> ScanData:
    try:
        with h5py_utils.File(filename) as h5:
            sino = SinogramReader(h5, sequence_path="1.1")
            model = TechniqueModelReader(h5, sequence_path="1.1")

            def get_pixel_size():
                dataset = h5["1.1/technique/optic/sample_pixel_size"]
                v = dataset[()]
                assert dataset.attrs["units"] == "um"  # FIXME: better implem expected
                return v

            def get_energy():
                dataset = h5["1.1/technique/scan/energy"]
                v = dataset[()]
                assert dataset.attrs["units"] == "keV"  # FIXME: better implem expected
                return v

            def get_distance():
                dataset = h5["1.1/technique/scan/sample_detector_distance"]
                v = dataset[()]
                assert dataset.attrs["units"] == "mm"  # FIXME: better implem expected
                return v * 0.001

            pixel_size = get_pixel_size() * 1e-6  # in micron
            distance = get_distance()  # in meter
            energy = get_energy()  # in keV
            q = model.y_axis
            y_axis = pint.Quantity(q[0], q[1])
            q = model.sample_pixel_size
            sample_pixel_size = pint.Quantity(q[0], q[1])
            q = model.detector_center_y
            detector_center_y = None if q is None else pint.Quantity(q[0], q[1])

            return ScanData(
                sino=sino,
                model=model,
                pixel_size=pixel_size,
                distance=distance,
                energy=energy,
                y_axis=y_axis,
                sample_pixel_size=sample_pixel_size,
                detector_center_y=detector_center_y,
            )
    except Exception:
        if TEST_CONTEXT or APP_CONTEXT:
            _logger.error("Error while reading file", exc_info=True)
        raise


class TomoSinogramReconstruction(
    Task,
    output_names=[],
    optional_input_names=[
        "sleep_time",
        "axisposition",
        "deltabeta",
        "backends",
        "cor_backend",
    ],
):
    def run(self) -> None:
        _logger.info("Start")
        _logger.info("Input values: %s", self.input_values)

        if self.inputs.sleep_time:
            time.sleep(self.inputs.sleep_time)

        metadata = self.input_values["metadata"]
        # "metadata": {
        #     "fileTemplate": "sample1_sample1_scan_0006.h5",
        #     "imageDirectory": "/tmp/scans/visitor/blc00001/id00/20221001/sample1/sample1_sample1_scan_0006",
        #     "imageContainerSubPath": None,
        #     "numberOfImages": None,
        #     "exposureTime": None,
        #     "runStatus": "Successful",
        #     "experimentType": "tomo",
        #     "beamLineName": "bl",
        #     "session": "blc00001-1",
        #     "proposal": "blc00001",
        #     "sequence": None,
        #     "steps_x": None,
        #     "steps_y": None,
        #     "workingDirectory": "/tmp/scans/visitor/blc00001/id00/20221001/sample1/sample1_sample1_scan_0006/sample1_sample1_scan_0006",
        # },
        # "taskDirectory": "/tmp/scans/visitor/blc00001/id00/20221001/sample1/sample1_sample1_scan_0006/sample1_sample1_scan_0006/processed/fa02ea10-7140-41e4-afcd-55db076e757c/tomo-sinogram-reconstruction",
        # "dataCollectionId": 8,
        # "processingJobId": 10,
        # "autoProcProgramId": 10,
        # "sleep_time": "<MISSING_DATA>",
        autoproc_program_id = self.input_values["autoProcProgramId"]
        data_collection_id = self.input_values["dataCollectionId"]

        tomovis_config = read_tomovis_config()

        def get_backends():
            backends = self.get_input_value("backends", None)
            if backends is not None:
                backends = backends.split(",")
                return backends

            backends = tomovis_config.get("slice_reconstruction", {}).get(
                "available_backends", None
            )

            if backends is not None:
                if not isinstance(backends, list):
                    raise ValueError(
                        "available_backends is expected to be a list of string"
                    )
                return backends

            return DEFAULT_BACKENDS

        def get_cor_backend():
            backend = self.get_input_value("cor_backend", None)
            if backend is not None:
                return backend

            backend = tomovis_config.get("slice_reconstruction", {}).get(
                "cor_backend", None
            )
            if backend is not None:
                return backend

            return DEFAULT_COR_BACKEND

        requested_axis_position = self.get_input_value("axisposition", None)
        requested_delta_beta = self.get_input_value("deltabeta", None)
        if requested_delta_beta is None:
            requested_delta_beta = 200.0
        requested_backends = get_backends()
        requested_cor_backend = get_cor_backend()

        _logger.info("Requested axis position: %s", requested_axis_position)
        _logger.info("Requested delta/beta: %s", requested_delta_beta)

        def get_output_directory():
            path = self.input_values.get("taskDirectory")
            if path is None:
                path = metadata.get("workingDirectory")
                if path is not None:
                    path = os.path.join(path, "tomo-sinogram-reconstruction")
            if path is None:
                raise RuntimeError("No taskDirectory or workingDirectory metadata")
            if not os.path.exists(path):
                try:
                    os.makedirs(path)
                except OSError:
                    _logger.error(
                        "Error while creating output directory", exc_info=True
                    )
                    raise
            return path

        def get_input_filename():
            path = metadata.get("imageDirectory")
            filename = metadata.get("fileTemplate")
            file_input = os.path.join(path, filename)
            if not os.path.exists(file_input):
                raise RuntimeError("No input file found: %s", file_input)
            return file_input

        output_directory = get_output_directory()
        input_filename = get_input_filename()
        output_filename = os.path.join(output_directory, "reconstructed.h5")

        _logger.info("Task ready")
        _logger.info("Beamline: %s", metadata.get("beamLineName"))
        _logger.info("Filename: %s", input_filename)

        _logger.info("Read data")
        with timeit() as read_time:
            try:
                scan_data = read_scan_data(input_filename)
            except Exception:
                _logger.error("Error while reading data", exc_info=True)
                raise

            sino_data = scan_data.sino
            model_data = scan_data.model
            pixel_size = scan_data.pixel_size
            distance = scan_data.distance
            energy = scan_data.energy

            _logger.info("Pixel size: %s um", pixel_size)
            _logger.info("Propagation distance: %s m", distance)
            _logger.info("Energy: %s keV", energy)

            translation_size = sino_data.translation_shape
            angles_deg = sino_data.rotation[0::translation_size]
            angles = numpy.deg2rad(angles_deg, dtype=numpy.float32)
            sinos = sino_data.sinogram.reshape(-1, translation_size)
            sinos = sinos.astype(numpy.float32)
        _logger.info("Read time: %ss", read_time.duration)

        _logger.info("Process data")

        estimated_axis_position = None
        with timeit() as guess_time:
            if requested_axis_position is None:
                estimated_axis_position = find_estimated_axis_position(
                    sinos,
                    angles_deg,
                    backend=requested_cor_backend,
                    y_axis=scan_data.y_axis,
                    sample_pixel_size=scan_data.sample_pixel_size,
                    detector_center_y=scan_data.detector_center_y,
                )
                _logger.info("Estimated axis position: %s", estimated_axis_position)
                if estimated_axis_position is None:
                    used_axis_position = sinos.shape[1] / 2
                else:
                    used_axis_position = estimated_axis_position
            else:
                used_axis_position = requested_axis_position
        _logger.info("Guess time: %ss", guess_time.duration)
        _logger.info("Used axis position: %s", used_axis_position)

        d1 = int(numpy.ceil(sinos.shape[1] - used_axis_position))
        d2 = int(numpy.ceil(used_axis_position))
        max_size = 2 * max(d1, d2)
        slice_shape = max_size, max_size

        def process_with_nabu():
            try:
                _logger.info("Nabu phase retrival")
                radios = numpy.array(sinos)
                radios.shape = sinos.shape[0], 1, translation_size

                paganin = PaganinPhaseRetrieval(
                    (1, translation_size),
                    distance,
                    energy,
                    requested_delta_beta,
                    pixel_size,
                )
                radios_after_phase = numpy.zeros_like(radios, order="C")
                for i, radio in enumerate(radios):
                    paganin.apply_filter(radio, output=radios_after_phase[i])

                log = Log(radios_after_phase.shape, clip_min=1e-6, clip_max=10.0)
                log.take_logarithm(radios_after_phase)  # in-place

                sinos_with_phase_retrieval = numpy.array(radios_after_phase)
                sinos_with_phase_retrieval.shape = sinos.shape

                _logger.info("Nabu back projector")
                fbp = Backprojector(
                    sinos_with_phase_retrieval.shape,
                    slice_shape=slice_shape,
                    rot_center=used_axis_position,
                    angles=angles,
                    padding_mode="edges",
                    extra_options={"centered_axis": True},
                )
            except Exception:
                _logger.error("Error while creating nabu back projector", exc_info=True)
                return None

            try:
                array = fbp.filtered_backprojection(sinos_with_phase_retrieval)
            except Exception:
                _logger.error(
                    "Error while computing the back projection", exc_info=True
                )
                raise

            return array

        def process_with_silx():
            fbp = backprojection.Backprojection(
                sino_shape=sinos.shape,
                slice_shape=slice_shape,
                angles=angles,
                axis_position=used_axis_position,
                devicetype="all",
                extra_options={
                    "gpu_offset_x": used_axis_position - (slice_shape[1] - 1) / 2.0,
                    "gpu_offset_y": used_axis_position - (slice_shape[0] - 1) / 2.0,
                },
            )
            try:
                array = fbp.filtered_backprojection(sinos)
            except Exception:
                _logger.error(
                    "Error while computing the back projection", exc_info=True
                )
                raise

            return array

        with timeit() as process_time:
            array = None
            for backend in requested_backends:
                _logger.info("Data processing with %s", backend)
                if backend == "nabu":
                    if not nabu:
                        _logger.info("Nabu library not available. Backend skipped")
                    else:
                        array = process_with_nabu()
                elif backend == "silx":
                    if not backprojection:
                        _logger.info("Silx library not available. Backend skipped")
                    else:
                        array = process_with_silx()
                elif backend == "dummy":
                    array = numpy.zeros((128, 128))
                else:
                    raise ValueError("Backend '%s' is not supported" % backend)
                if array is not None:
                    break
            else:
                raise ValueError("No backend available. The data was not processed")
        _logger.info("Process time: %ss", process_time.duration)

        def store_quantity(h5, name, quantity, unit=None):
            if quantity is None:
                return
            if isinstance(quantity, tuple):
                value = quantity[0]
                if unit is None:
                    unit = quantity[1]
            else:
                value = quantity
            h5[name] = value
            if unit is not None:
                h5[name].attrs["units"] = unit

        _logger.info("Write to %s", output_filename)
        with timeit() as save_time:
            with h5py.File(output_filename, mode="a") as h5:
                h5["/ending/array"] = array.astype(numpy.float16)
                h5["/ending"].attrs["NX_class"] = "NXdata"
                h5["/ending"].attrs["signal"] = "array"
                store_quantity(h5, "/ending/x_axis", model_data.x_axis)
                store_quantity(h5, "/ending/y_axis", model_data.y_axis)
                store_quantity(h5, "/ending/sample_x_axis", model_data.sample_x_axis)
                store_quantity(h5, "/ending/sample_y_axis", model_data.sample_y_axis)
                store_quantity(
                    h5, "/ending/sample_pixel_size", model_data.sample_pixel_size
                )
                store_quantity(
                    h5, "/ending/estimated_axis_position", estimated_axis_position, "px"
                )
                store_quantity(
                    h5, "/ending/used_axis_position", used_axis_position, "px"
                )
                store_quantity(h5, "/ending/delta_beta", requested_delta_beta)
                h5["/ending/backend"] = backend
                h5["/ending/cor_backend"] = requested_cor_backend

                normalized_array = array[numpy.isfinite(array)]

                histo, bins = numpy.histogram(normalized_array, 256)
                h5["/ending/histo/counts"] = histo.astype(numpy.float32)
                h5["/ending/histo/bin_edges"] = bins.astype(numpy.float32)

                def float_or_none(v):
                    return numpy.nan if v is None else float(v)

                minmax = min_max(normalized_array, min_positive=True, finite=True)
                h5["/ending/stats/min"] = float_or_none(minmax.minimum)
                h5["/ending/stats/min_positive"] = float_or_none(minmax.min_positive)
                h5["/ending/stats/max"] = float_or_none(minmax.maximum)
                h5["/ending/stats/mean"] = float(numpy.mean(normalized_array))
                h5["/ending/stats/std"] = float(numpy.std(normalized_array))

                store_quantity(h5, "/ending/read_time", read_time.duration, "s")
                store_quantity(h5, "/ending/guess_time", guess_time.duration, "s")
                store_quantity(h5, "/ending/process_time", process_time.duration, "s")

        _logger.info("Save time: %ss", save_time.duration)

        _logger.info("Send events")

        _autoproc_attachment_id = add_autoproc_attachment(
            autoproc_program_id,
            os.path.dirname(output_filename),
            os.path.basename(output_filename),
            "Result",
            1,
        )

        notify(
            metadata["beamLineName"],
            {
                "type": "tomo-sinogram-reconstruction",
                "datacollectionid": data_collection_id,
                "autoprocprogramid": autoproc_program_id,
            },
        )

        _logger.info("Done")
