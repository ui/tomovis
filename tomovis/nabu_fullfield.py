# coding: utf-8
# License: MIT (c) ESRF 2021
"""nabu fullfield reconstruction REST API"""
import configparser
import logging
import pathlib
import sys
from typing import List, Optional

import flask
import flask_restful

from .crud_handlers import CrudBlueprint, SubprocessExecutorManager
from .schemas import NabuFullfieldConfigSchema, NabuFullfieldSchema

_logger = logging.getLogger(__name__)


def save_nabu_config(filepath: pathlib.Path, config: dict):
    config_parser = configparser.ConfigParser()
    config_parser.read_dict(config)
    with filepath.open("w") as f:
        config_parser.write(f)


class ProcessManager(SubprocessExecutorManager):
    CONFIG_FILE_NAME = "config.ini"

    def __init__(self, base_path: pathlib.Path):
        _logger.info("Start nabu_fullfield execution manager")
        super().__init__(base_path)

    def config_file_path(self, id_: str) -> pathlib.Path:
        """Returns nabu config file path for given id"""
        return self.dir_path(id_) / self.CONFIG_FILE_NAME

    def command(self, id_: str, config: dict) -> List[str]:
        config_path = self.config_file_path(id_)
        save_nabu_config(config_path, config)

        return [
            sys.executable,
            "-m",
            "nabu.app.reconstruct",
            str(config_path),
        ]

    def __setitem__(self, id_: str, config: dict) -> None:
        full_config = config.copy()
        full_config["output"] = dict(
            location=str(self.dir_path(id_)),
            file_prefix="output",
            file_format="hdf5",
            overwrite_results="0",
        )
        full_config["pipeline"] = dict(verbosity="3")
        super().__setitem__(id_, full_config)

    def __getitem__(self, id_: str) -> dict:
        hdf5_filename = f"nabu_fullfield/{id_}/output.hdf5"
        hdf5_entry = "/entry0000/reconstruction/results/data"

        content = super().__getitem__(id_)
        content.update(
            dict(
                links=[
                    dict(
                        rel="data",
                        action="GET",
                        href=f"{flask.request.root_url}h5grove/data/?file={hdf5_filename}&path={hdf5_entry}&format=npy",
                    ),
                    dict(
                        rel="config",
                        action="GET",
                        href=f"{flask.request.base_url}/config.ini",
                    ),
                    dict(
                        rel="h5web",
                        action="GET",
                        href=f"{flask.request.root_url}/h5web?filepath={hdf5_filename}",
                    ),
                ],
            )
        )
        return content


class ConfigFile(flask_restful.Resource):
    """Entry point to get nabu config file"""

    def __init__(self, manager: ProcessManager):
        super().__init__()
        self.__manager = manager

    def get(self, resource_id: int):
        return flask.send_file(str(self.__manager.config_file_path(resource_id)))


class NabuFullfieldBlueprint(CrudBlueprint):
    """Flask blueprint for nabu reconstructions end point"""

    def __init__(self, *args, base_dir: Optional[pathlib.Path] = None, **kwargs):
        assert base_dir is not None

        # Create subdirectory to store reconstructions
        path = base_dir / "nabu_fullfield"
        path.mkdir(parents=True, exist_ok=True)

        manager = ProcessManager(path)
        super().__init__(
            manager, NabuFullfieldConfigSchema, NabuFullfieldSchema, *args, **kwargs
        )
        self.api.add_resource(
            ConfigFile,
            "/<int:resource_id>/config.ini",
            resource_class_args=(manager,),
        )
