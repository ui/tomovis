import pathlib

RESOURCES_PATH = pathlib.Path(__file__).parent
STATIC_PATH = RESOURCES_PATH / "static"
