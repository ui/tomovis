# coding: utf-8
# License: MIT (c) ESRF 2021
"""Test SubprocessExecutor class"""
import time
import pytest

from tomovis.subprocess_executor import SubprocessExecutor


def test_simple_execution():
    command = ["sleep", "1"]
    with SubprocessExecutor() as executor:
        future = executor.submit(command)
        completed_process = future.result()
        assert completed_process.args == command
        assert completed_process.returncode == 0


def test_queued_execution():
    command = ["sleep", "1"]
    with SubprocessExecutor() as executor:
        future1 = executor.submit(command)
        future2 = executor.submit(command)
        assert not future2.running() and not future2.done()

        while not future1.done():
            time.sleep(0.1)
        # Right after command 1 has run, command 2 is not done yet
        assert not future2.done()

        completed_process1 = future1.result()
        assert completed_process1.args == command
        assert completed_process1.returncode == 0

        completed_process2 = future2.result()
        assert completed_process2.args == command
        assert completed_process2.returncode == 0


def test_cancel_before_running():
    command = ["sleep", "3"]
    with SubprocessExecutor() as executor:
        future1 = executor.submit(command)

        future2 = executor.submit(command)
        assert not future2.running() and not future2.done()
        future2.cancel()
        assert future2.cancelled()
        assert not future1.cancelled() and not future1.done()
        future1.cancel()
        assert future1.cancelled()


def test_cancel_while_running():
    command = ["sleep", "10"]
    with SubprocessExecutor() as executor:
        future = executor.submit(command)
        # Wait for command to be running
        while not future.running() and not future.done():
            time.sleep(0.1)
        assert future.running()

        future.cancel()
        assert future.cancelled()


def test_shutdown_nowait():
    command = ["sleep", "1"]
    executor = SubprocessExecutor()
    future1 = executor.submit(command)
    future2 = executor.submit(command)

    while not future1.running():
        time.sleep(0.1)

    executor.shutdown(wait=False)
    assert future1.running()
    assert not future2.running() and not future2.done()

    completed_process1 = future1.result()
    assert completed_process1.args == command
    assert completed_process1.returncode == 0

    completed_process2 = future2.result()
    assert completed_process2.args == command
    assert completed_process2.returncode == 0


@pytest.mark.parametrize("wait", (True, False))
def test_shutdown_cancel_futures(wait):
    command = ["sleep", "5"]
    executor = SubprocessExecutor()
    future1 = executor.submit(command)
    future2 = executor.submit(command)

    while not future1.running():
        time.sleep(0.1)

    executor.shutdown(wait=wait, cancel_futures=True)
    assert future1.cancelled()
    assert future2.cancelled()
