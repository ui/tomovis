# coding: utf-8
# License: MIT (c) ESRF 2021
"""Test mock server for BM18 reconstruction/visualization"""

import json
import pytest


@pytest.mark.parametrize(
    "server",
    [
        pytest.lazy_fixture("subprocess_server"),
        pytest.lazy_fixture("flask_server"),
    ],
)
def test_nabu_fullfield(server):
    # Create new reconstruction
    params = dict(
        dataset=dict(
            location="/path/to/h5",
            binning=4,
            binning_z=4,
            projections_subsampling=4,
        ),
        phase=dict(
            method="paganin",
            delta_beta=1,
            unsharp_coeff=0.5,
            unsharp_sigma=1,
        ),
        reconstruction=dict(
            rotation_axis_position="centered",
            enable_halftomo=False,
        ),
        postproc=dict(
            output_histogram=True,
            histogram_bins=10000,
        ),
    )
    response = server.post("/nabu_fullfield/", data=params)
    assert response.status == 202

    content = json.loads(response.content)
    id_ = content["id"]
    assert content["status"] in ("pending", "running")

    # Get list of all reconstructions
    response = server.get("/nabu_fullfield/")
    content = json.loads(response.content)
    items = content["items"]
    assert response.status == 200
    assert len(items) == 1
    assert items[0]["id"] == id_
    assert "config" in items[0]

    # Get reconstruction info for id_
    response = server.get(f"/nabu_fullfield/{id_}")
    content = json.loads(response.content)
    assert response.status == 200
    assert content["id"] == id_

    # Delete reconstruction info and associated data
    response = server.request(f"/nabu_fullfield/{id_}", method="DELETE")
    assert response.status == 204

    # Get list of all reconstructions
    response = server.get("/nabu_fullfield/")
    content = json.loads(response.content)
    items = content["items"]
    assert response.status == 200
    assert len(items) == 0
