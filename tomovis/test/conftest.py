# coding: utf-8
# License: MIT (c) ESRF 2021

import json
import os
import pathlib
import socketserver
import subprocess
import sys
import time
from typing import Callable, List, NamedTuple, Optional, Tuple
import urllib.request
from urllib.error import HTTPError

import pytest

import tomovis.server


class BaseServer:
    """Base class for object provided through `server` fixture"""

    class Response(NamedTuple):
        """Return type of :meth:`get`"""

        status: int
        headers: List[Tuple[str, str]]
        content: bytes

    def __init__(self, served_dir: pathlib.Path):
        self.__served_dir = served_dir

    @property
    def served_directory(self) -> pathlib.Path:
        """Root directory served by the running server"""
        return self.__served_dir

    def _request(
        self, url: str, method: str, data: Optional[bytes], headers, benchmark: Callable
    ) -> Response:
        """Override in subclass to implement fetching response"""
        raise NotImplementedError()

    @staticmethod
    def __default_benchmark(function):
        return function()

    def request(
        self,
        url: str,
        method: str = "GET",
        data=None,
        benchmark: Optional[Callable] = None,
    ) -> Response:
        """Request url and return retrieved response"""
        if benchmark is None:
            benchmark = self.__default_benchmark

        if data is None or isinstance(data, bytes):
            encoded_data = data
            headers = {}
        elif isinstance(data, str):
            encoded_data = data.encode("utf-8")
            headers = {}
        else:
            encoded_data = json.dumps(data).encode("utf-8")
            headers = {"Content-Type": "application/json"}
        response = self._request(url, method, encoded_data, headers, benchmark)

        content_lengths = [
            header[1] for header in response.headers if header[0] == "Content-Length"
        ]
        if content_lengths:
            assert len(response.content) == int(content_lengths[0])

        return response

    def get(
        self,
        url: str,
        benchmark: Optional[Callable] = None,
    ) -> Response:
        """GET request that returns retrieved response"""
        return self.request(url, method="GET", data=None, benchmark=benchmark)

    def post(self, url: str, data, benchmark: Optional[Callable] = None) -> Response:
        """POST request that returns retrieved response"""
        return self.request(url, method="POST", data=data, benchmark=benchmark)

    def assert_404(self, url: str):
        raise NotImplementedError()


# subprocess_server fixture  ###


class _SubprocessServer(BaseServer):
    """Class of objects provided through subprocess-based `server` fixture"""

    def __init__(self, served_dir: pathlib.Path, base_url):
        super().__init__(served_dir)
        self.__base_url = base_url

    def _request(
        self, url: str, method: str, data, headers, benchmark: Callable
    ) -> BaseServer.Response:
        request = urllib.request.Request(
            self.__base_url + url, data=data, headers=headers, method=method
        )
        r = benchmark(lambda: urllib.request.urlopen(request))
        return BaseServer.Response(
            status=r.status, headers=r.headers.items(), content=r.read()
        )

    def assert_404(self, url: str):
        with pytest.raises(HTTPError) as e:
            self._request(url, lambda f: f())
            assert e.value.code == 404


def get_free_tcp_port(host: str = "localhost") -> int:
    """Returns an available TCP port"""
    with socketserver.TCPServer((host, 0), socketserver.BaseRequestHandler) as s:
        return s.server_address[1]


@pytest.fixture(scope="module")
def subprocess_server(tmp_path_factory):
    """Fixture running server as a subprocess.

    Provides a function to fetch endpoints from the server.
    """
    base_dir = tmp_path_factory.mktemp("server_base_dir").absolute()

    project_root_dir = pathlib.Path(__file__).absolute().parent.parent
    host = "localhost"
    port = str(get_free_tcp_port(host))
    cmd = [
        sys.executable,
        "-m",
        "tomovis",
        "--host",
        host,
        "--port",
        port,
        "--basedir",
        f"{str(base_dir)}",
    ]
    env = os.environ.copy()
    env["PYTHONPATH"] = f"{str(project_root_dir)}:{env.get('PYTHONPATH', '')}"
    process = subprocess.Popen(cmd, env=env)
    time.sleep(1)
    assert process.poll() is None  # Check that server is running

    yield _SubprocessServer(served_dir=base_dir, base_url=f"http://{host}:{port}")

    process.kill()
    assert process.wait(timeout=4) is not None  # Check that server is stopped


# Flask server fixture


class _FlaskServer(BaseServer):
    """Class of objects provided by `flask_server` fixture"""

    def __init__(self, served_dir: pathlib.Path, client):
        super().__init__(served_dir)
        self.__client = client

    def _request(
        self, url: str, method: str, data, headers, benchmark: Callable
    ) -> BaseServer.Response:
        r = benchmark(
            lambda: self.__client.open(url, data=data, headers=headers, method=method)
        )
        return BaseServer.Response(
            status=r.status_code, headers=list(r.headers), content=r.get_data()
        )

    def assert_404(self, url: str):
        response = self._get_response(url, lambda f: f())
        assert "Not Found" in str(response.content)
        assert response.status == 404


@pytest.fixture(scope="session")
def flask_server(tmp_path_factory):
    """Flask test client-based `server` fixture.

    Provides a function to fetch endpoints from the server.
    """
    base_dir = tmp_path_factory.mktemp("server_base_dir").absolute()
    app = tomovis.server.create_app(base_dir, "Test server")
    with app.test_client() as client:
        yield _FlaskServer(base_dir, client)
