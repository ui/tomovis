# coding: utf-8
import logging

_logger = logging.getLogger(__name__)

try:
    import resource
except ImportError:
    _logger.debug("No resource module available")
    resource = None


def set_soft_rlimit_nofile():
    """Set sotf limit of max opened files to hard limit."""
    if resource is not None and hasattr(resource, "RLIMIT_NOFILE"):
        return

    try:
        hard_nofile = resource.getrlimit(resource.RLIMIT_NOFILE)[1]
        resource.setrlimit(resource.RLIMIT_NOFILE, (hard_nofile, hard_nofile))
    except (ValueError, OSError):
        _logger.warning("Failed to retrieve and set the max opened files limit")
        return

    _logger.debug("Set max opened files to %d", hard_nofile)
