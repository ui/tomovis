from flufl.lock import Lock


class ReentrantLock(Lock):
    def __init__(self, *args, **kwargs) -> None:
        super().__init__(*args, **kwargs)
        self.__context_count = 0

    def __enter__(self) -> "ReentrantLock":
        self.__context_count += 1
        if self.__context_count == 1:
            return super().__enter__()
        return self

    def __exit__(self, *exc) -> bool:
        self.__context_count -= 1
        if self.__context_count == 0:
            return super().__exit__(*exc)
        return False
