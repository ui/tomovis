#!/usr/bin/env python
# coding: utf-8
# License: MIT (c) ESRF 2021

import traceback
from silx.utils import retry as retry_mod


def _is_h5py_exception(e):
    for frame in traceback.walk_tb(e.__traceback__):
        if frame[0].f_globals.get("__package__", None) == "h5py":
            return True
    return False


def _retry_h5py_error(e):
    """
    :param BaseException e:
    :returns bool:
    """
    if _is_h5py_exception(e):
        if isinstance(e, (OSError, RuntimeError)):
            return True
        elif isinstance(e, KeyError):
            # For example this needs to be retried:
            # KeyError: 'Unable to open object (bad object header version number)'
            return "Unable to open object" in str(e)
    elif isinstance(e, retry_mod.RetryError):
        return True
    return False


def retry_on_error(e):
    """Add IndexError and missing file to retry exceptions"""
    # if h5py_utils._is_h5py_exception(e):
    if _is_h5py_exception(e):
        if isinstance(e, (FileNotFoundError, OSError)):
            return True
    if isinstance(e, IndexError):
        return True
    # return h5py_utils._retry_h5py_error(e)
    return _retry_h5py_error(e)
