# Fullfield tomo reconstruction and visualisation server prototype

## Idea

Run reconstruction, visualisation pre-processing and serve visualisation data from one machine as a single standalone service.

## Managing Runtime Environment

To create an conda environment to run `tomovis`, use:

`mamba env create --file environment-lock.yml -n <env_name>`

Warning: `tomovis` is NOT installed in this environment, only its dependencies.

The files `environment.yml` and `environment-lock.yml` describe the conda environment to use for running `tomovis`:

- `environment.yml`: List of conda/pip dependencies without version pinning.
  This installs as much package as possible from conda.

  This is generated with: `mambda env export --from-history > environment.yml` with manual editing for packages installed from git:
  ```
      - git+https://gitlab.esrf.fr/tomo/ebs-tomo@master
      - git+https://github.com/stufisher/python-zocalo@main-external
  ```
- `environment-lock.yml`: Full list of conda/pip packages with pinned version needed to recreate the environment.

  This is generated with: `mamba env export --no-builds > environment-lock.yml` with manual editing for packages installed from git:
  ```
      - git+https://gitlab.esrf.fr/tomo/ebs-tomo@<commit hash or tag>
      - git+https://github.com/stufisher/python-zocalo@<commit hash or tag>
  ```

## Tests

The tests are launched with `pytest`. An extra argument allow to specify the location
of the resources used to test.

```
pytest --testdata ./testdata
```

To test the package and not the source code, an extra plugin have to be loaded first.

```
PYTEST_PLUGINS=tomovis.pytest_plugin pytest --pyargs tomovis --testdata ../testdata
```

## Tagging a version

Use [bump2version](https://pypi.org/project/bump2version/) to manage the version and tag, with this commands:

```
bump2version [major|minor|patch]
git push && git push --tags
```

## Endpoints

It provides 2 kinds of endpoints:
- Manage processing: `/image_tiling/`, `/nabu_fullfield/`
- HDF5 data access via [h5grove](https://github.com/silx-kit/h5grove): `/h5grove/`

See the [API Documentation](doc/openapi.json)
