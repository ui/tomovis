#!/usr/bin/env python
# coding: utf-8
# License: MIT (c) ESRF 2021
"""Sample code to reconstruct a dataset with nabu"""
import json
import argparse
import socket
import time
from urllib.request import Request, urlopen


NABU_CONFIG = dict(
    dataset=dict(
        location="/media/nvme/tvincent/bamboo/bamboo_nxtomomill.h5",
    ),
    preproc=dict(
        flatfield=1,
        ccd_filter_enabled=1,
        ccd_filter_threshold=0.04,
        double_flatfield_enabled=0,
        take_logarithm=1,
    ),
    phase=dict(
        method="paganin",
        delta_beta=100.0,
        unsharp_coeff=0,
        unsharp_sigma=0,
    ),
    reconstruction=dict(
        method="FBP",
        rotation_axis_position="centered",
        padding_type="zeros",
        enable_halftomo=True,
        start_x=0,
        end_x=-1,
        start_y=0,
        end_y=-1,
        start_z=0,
        end_z=0,
    ),
    postproc=dict(output_histogram=True),
)


IMAGE_TILING_CONFIG = dict(
    dataset=dict(
        location="/media/nvme/tvincent/tiling_2021-11-03/id002111_sample.h5",
        hdf5_entry="/sample_0001_1.1",
    ),
)


parser = argparse.ArgumentParser(description=__doc__)
parser.add_argument(
    "-p", "--port", type=int, default=8888, help="Port to send requests to"
)
parser.add_argument(
    "--host", default=socket.gethostname(), help="Host where to send requests"
)
parser.add_argument(
    "requests",
    default=["nabu", "tiling"],
    nargs="*",
    help="Kind of requests to request: 'nabu', 'tiling'",
)
options = parser.parse_args()
base_url = f"http://{options.host}:{options.port}"


def create_resource(endpoint: str, config: str):
    print(f">>> POST {endpoint}", json.dumps(config, indent=4))
    response = urlopen(
        Request(
            endpoint,
            data=json.dumps(config).encode("utf-8"),
            headers={"Content-Type": "application/json"},
            method="POST",
        )
    )
    assert response.status == 202
    content = json.loads(response.read())
    id_ = content["id"]
    status = content["status"]
    print("<<<", response.status, json.dumps(content, indent=4))

    # Poll status
    url = f"{endpoint}{id_}"
    while status != "done":
        time.sleep(1)
        response = urlopen(url)
        assert response.status == 200
        content = json.loads(response.read())
        status = content["status"]
        print(f"... status: {status}")
    print(f">>> GET {url}")
    print("<<<", response.status, json.dumps(content, indent=4))


for request_name in options.requests:
    endpoint_name, config = dict(
        nabu=("nabu_fullfield/", NABU_CONFIG),
        tiling=("image_tiling/", IMAGE_TILING_CONFIG),
    )[request_name]
    url = f"{base_url}/{endpoint_name}"
    print(f"Request {endpoint_name}: {url}")
    create_resource(url, config)
