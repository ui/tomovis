import { App, H5GroveProvider } from "@h5web/app";
import { useLocation } from "react-router-dom";

const URL = "/h5grove/";

export function useQueryParam(name: string): string | null {
  const query = new URLSearchParams(useLocation().search);
  return query.get(name);
}

function Viewer() {
  const filepath = useQueryParam("filepath");

  if (!filepath) {
    return <div>Nothing to see here, provide a filepath</div>;
  }
  return (
    <H5GroveProvider
      url={URL}
      filepath={filepath}
      axiosParams={{ file: filepath }}
    >
      <App />
    </H5GroveProvider>
  );
}

export default Viewer;
