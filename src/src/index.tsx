import "@h5web/app/dist/style-lib.css";
import "@h5web/app/dist/style.css";
import "./styles.css";

import { StrictMode } from "react";
import ReactDOM from "react-dom";
import { BrowserRouter } from "react-router-dom";

import Viewer from "./Viewer";

ReactDOM.render(
  <StrictMode>
    <BrowserRouter>
      <Viewer />
    </BrowserRouter>
  </StrictMode>,
  document.getElementById("root")
);
